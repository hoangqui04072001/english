@extends('admin.layout.app')
@section('title', 'Tạo Bài Viết Blog')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Blog</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Create Blog</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Tạo Bài Viết Blog</h2>
        </div>

        <div>
            <form action="{{ route('blog.store') }}" method="POST">
                @csrf
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tiêu Đề</label>
                        <input type="text" value="{{ old('title') }}" name="title" class="form-control">
                        @error('title')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Mô Tả Blog</label><br>
                        <textarea name="description" id="editor" class="form-control"></textarea>

                        @error('description')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Tên Người tạo</label>
                        <select name="categoryblog" class="form-control">
                            <option>---Chọn---</option>
                            <option value="Tiếng Anh Mỗi Ngày">Tiếng Anh Mỗi Ngày</option>
                            <option value="Làm Các Bài Tập Tiếng Anh">Làm Các Bài Tập Tiếng Anh</option>
                            <option value="1000 Từ Vựng Mỗi Ngày">1000 Từ Vựng Mỗi Ngày</option>
                        </select>
                        @error('categoryblog')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label>Tên Người tạo</label>
                        <input type="text" value="{{ old('name') }}" name="name" class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>
                    <div class="row">
                        <div class="input-group-static col-5 mb-4">
                            <label>Hình Ảnh</label>
                            <input type="text" name="image" id="image_url" class="form-control"
                                placeholder="đường dẫn url hình ảnh">
                            @error('image')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror

                        </div>
                        <div class="col-5">
                            <img src="" id="show-image" width="200" height="200">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.ckeditor.com/ckeditor5/37.0.1/classic/ckeditor.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.2.min.js"></script>
    <script>
        document.getElementById('picture').addEventListener('change', function(event) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('show-image').src = e.target.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        });
    </script>
    <script>
        // Khởi tạo CKEditor trên thẻ textarea có id là "editor"
        ClassicEditor.create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        // Lắng nghe sự kiện thay đổi trường nhập liệu đường dẫn URL
        $('#image_url').on('change', function() {
            // Lấy giá trị đường dẫn URL từ trường nhập liệu
            var imageUrl = $('#image_url').val();
            // Đặt đường dẫn URL vào thuộc tính "src" của thẻ <img>
            $('#show-image').attr('src', imageUrl);
        });
    </script>
@endsection
