@extends('admin.layout.app')
@section('title', 'Bài Viết Blog')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Blog</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Blog</h6>
    </nav>
@endsection

@section('content')
    <div class="card">
        @if (session('message'))
            <div>
                <h3 class="alert alert-success text-white">{{ session('message') }}</h3>
            </div>
            <script>
                setTimeout(function() {
                    document.querySelector('.alert').style.display = 'none';
                }, 3000);
            </script>
        @endif
        <h1>
            Bài Viết Blog
        </h1>
        <div>
            <a style=" padding: 20px 25px; font-size: 20px;" href="{{ route('blog.create') }}"
                class="btn btn-primary">Thêm</a>
        </div>
        <div class="input-group input-group-outline my-3">
            <label class="form-label">Search</label>
            <input id="searchInput" type="text" class="form-control">
        </div>
        <div>
            <table class="table table-hover" style=" font-size: 1.4em; ">
                <tr>
                    <th>#</th>
                    <th>Tiêu Đề</th>
                    <th>Danh Mục Blog</th>
                    <th>Tên Người tạo</th>
                    <th>hình Ảnh</th>
                    <th>Action</th>
                </tr>
                @foreach ($blogs as $blog)
                    <tr>
                        <td>{{ $blog->id }}</td>
                        <td>{{ $blog->title }}</td>
                        <td>{{ $blog->categoryblog }}</td>
                        <td>{{ $blog->name }}</td>
                        <td><img src="{{ asset($blog->image) }}" width="80" height="80"></td>
                        <td>
                            <a href="{{ route('blog.edit', $blog->id) }}" class=" btn btn-warning btn-lg ">Edit</a>

                            <form action="{{ route('blog.destroy', $blog->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button class=" btn btn-danger btn-lg ">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div>
                {{ $blogs->links() }}
            </div>
        </div>

    </div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#searchInput').on('input', function() {
            var searchText = $(this).val().toLowerCase();
            $("table tbody tr").filter(function() {
                var title = $(this).find('td:eq(1)').text().toLowerCase();
                var name = $(this).find('td:eq(3)').text().toLowerCase();
                $(this).toggle(title.indexOf(searchText) > -1 || name.indexOf(searchText) > -1);
            });
        });
    });
</script>
@endsection
