@extends('admin.layout.app')
@section('title', 'Tạo Người Dùng')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">User</li>
        </ol>
        <h6 class="font-weight-bolder mb-0"> Tạo Người Dùng</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Tạo Người dùng</h2>
        </div>

        <div>
            <form action="{{ route('user.store') }}" method="POST">
                @csrf
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên</label>
                        <input type="text" value="{{ old('name') }}" name="name" class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Email</label>
                        <input type="email" value="{{ old('email') }}" name="email" placeholder="email"
                            class="form-control">

                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Mật Khẩu</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                            placeholder="Mật khẩu">

                        @error('password')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Điện Thoại</label>
                        <input type="tel" value="{{ old('phone') }}" name="phone" class="form-control">

                        @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Địa Chỉ</label>
                        <input type="text" value="{{ old('address') }}" name="address" class="form-control">

                        @error('address')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Căn Cước Công Dân</label>
                        <input type="tel" value="{{ old('cccd') }}" name="cccd" class="form-control">

                        @error('cccd')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Ngày Sinh</label>
                        <input type="date" value="{{ old('birth_date') }}" name="birth_date" class="form-control">

                        @error('birth_date')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Loại Người Dùng</label>
                        <select name="type" class="form-control">
                            <option value="">---Chọn---</option>
                            <option value="Admin">Quản Trị Viên</option>
                            <option value="Student">Học Viên</option>
                            <option value="Teacher">Giáo Viên</option>
                            <option value="Client">Khách Hàng</option>
                        </select>

                        @error('type')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Gender</label>
                        <select name="gender" class="form-control">
                            <option value="Nam">Nam</option>
                            <option value="Nữ">Nữ</option>
                            <option value="Khác">Khác</option>

                        </select>

                        @error('gender')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for=""> Vai Trò Người Dùng</label>
                    @foreach ($roles as $groupName => $role)
                        <div class="col-6">
                            <h4>{{ $groupName }}</h4>

                            <div>
                                @foreach ($role as $item)
                                    <div class="form-check">
                                        <input class="form-check-input" name="role_id[]" type="checkbox"
                                            value="{{ $item->id }}">
                                        <label class="custom-control-label"
                                            for="customCheck1">{{ $item->display_name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>
    </div>

@endsection
