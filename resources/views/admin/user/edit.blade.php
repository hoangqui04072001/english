@extends('admin.layout.app')
@section('title', 'sửa Người Dùng' . $user->name)

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">User</li>
        </ol>
        <h6 class="font-weight-bolder mb-0"> Sửa Người Dùng</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Sửa Người dùng</h2>
        </div>

        <div>
            <form action="{{ route('user.update', $user->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên</label>
                        <input type="text" value="{{ old('name') ?? $user->name }} " name="name" class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Email</label>
                        <input type="email" value="{{ old('email') ?? $user->email }}" name="email" placeholder="email"
                            class="form-control">

                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label for="password">Mật khẩu</label>
                        <input type="password" name="password" id="password"
                            class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}"
                            placeholder="Mật khẩu">

                        @error('password')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror


                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Điện Thoại</label>
                        <input type="tel" value="{{ old('phone') ?? $user->phone }}" name="phone"
                            class="form-control">

                        @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Địa Chỉ</label>
                        <input type="text" value="{{ old('address') ?? $user->address }}" name="address"
                            class="form-control">

                        @error('address')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Căn Cước Công Dân</label>
                        <input type="tel" value="{{ old('cccd') ?? $user->cccd }}" name="cccd" class="form-control">

                        @error('cccd')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Ngày Sinh</label>
                        <input type="date" value="{{ old('birth_date', date('Y-m-d')) }}" name="birth_date"
                            class="form-control">

                        @error('birth_date')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Loại Người Dùng</label>
                        <select name="type" class="form-control" value="{{ $user->type }}">
                            <option value="Admin">---Chọn---</option>
                            <option value="Admin" {{ old('type', $user->type) == 'Admin' ? 'selected' : '' }}>Quản Trị
                                Viên
                            </option>
                            <option value="Student" {{ old('type', $user->type) == 'Student' ? 'selected' : '' }}>Học Viên
                            </option>
                            <option value="Teacher" {{ old('type', $user->type) == 'Teacher' ? 'selected' : '' }}>Giáo Viên
                            </option>
                            <option value="Client" {{ old('type', $user->type) == 'Client' ? 'selected' : '' }}>Khách Hàng
                            </option>

                        </select>

                        @error('type')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Gender</label>
                        <select name="gender" class="form-control" value="{{ $user->gender }}">
                            <option>Nam</option>
                            <option>Nữ</option>
                            <option>Khác</option>

                        </select>

                        @error('gender')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for=""> Vai Trò Người Dùng</label>
                    @foreach ($roles as $groupName => $role)
                        <div class="col-6">
                            <h4>{{ $groupName }}</h4>

                            <div>
                                @foreach ($role as $item)
                                    <div class="form-check">
                                        <input class="form-check-input" name="role_id[]"
                                            {{ $user->roles->contains('id', $item->id) ? 'checked' : '' }} type="checkbox"
                                            value="{{ $item->id }}">
                                        <label class="custom-control-label"
                                            for="customCheck1">{{ $item->display_name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach

                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>
    </div>
@endsection
