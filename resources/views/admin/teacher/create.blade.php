@extends('admin.layout.app')
@section('title', 'Thêm Giáo Viên')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Teacher</li>
        </ol>
        <h6 class="font-weight-bolder mb-0"> Thêm Giáo Viên</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Thêm Giáo Viên</h2>
        </div>

        <div>
            <form action="{{ route('teacher.store') }}" method="POST">
                @csrf
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên</label>
                        <input type="text" value="{{ old('name') }}" name="name" class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Email Tài Khoản</label>
                        <select type="text" name="user_id" class="form-control">
                            <option value="">---Chọn---</option>
                            @foreach ($useremail as $item)
                                <option value="{{ $item->id }}">{{ $item->email }}</option>
                            @endforeach
                        </select>
                        @error('user_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Email</label>
                        <input type="email" value="{{ old('email') }}" name="email" placeholder="email"
                            class="form-control">

                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Địa Chỉ</label>
                        <input type="text" class="form-control @error('address') is-invalid @enderror" name="address">

                        @error('address')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Ngày Sinh</label>
                        <input type="date" value="{{ old('birth_date') }}" name="birth_date" class="form-control">

                        @error('birth_date')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Trạng Thái</label>
                        <select name="status" class="form-control">
                            <option>---Chọn---</option>
                            <option value="1">Có Lớp</option>
                            <option value="0">Không Có Lớp</option>

                        </select>
                        @error('status')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Điện Thoại</label>
                        <input type="tel" value="{{ old('phone') }}" name="phone" class="form-control">

                        @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Trình Độ English</label>
                        <select name="level" class="form-control">
                            <option>---Chọn---</option>
                            <option value="C1">Tiếng Anh Bậc C1</option>
                            <option value="C2">Tiếng Anh Bậc B2</option>
                            <option value="Ielts trên 7.0">Ielts 7.0 trở lên</option>
                            <option value="Toeic Trên 700">Toeic 700 trở lên </option>
                        </select>
                        @error('cccd')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người tạo</label>
                        <select name="created_by" class="form-control">
                            <option value="admin">Admin</option>
                            <option value="staff">Staff</option>


                        </select>

                        @error('created_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người Cập Nhật</label>
                        <select name="updated_by" class="form-control">
                            <option value="admin">Admin</option>
                            <option value="staff">Staff</option>


                        </select>

                        @error('updated_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>
    </div>

@endsection
