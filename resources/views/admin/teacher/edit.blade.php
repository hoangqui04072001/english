@extends('admin.layout.app')
@section('title', 'Sửa Giáo Viên: ' . $teacher->name)

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Teacher</li>
        </ol>
        <h6 class="font-weight-bolder mb-0"> Sửa Giáo Viên</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Sửa Thông Tin Giáo Viên</h2>
        </div>

        <div>
            <form action="{{ route('teacher.update', $teacher->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên</label>
                        <input type="text" value="{{ old('name') ?? $teacher->name }}" name="name"
                            class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Email Tài Khoản</label>
                        <select type="text" name="user_id" class="form-control">
                            <option value="">---Chọn---</option>
                            @foreach ($useremail as $item)
                                <option
                                    value="{{ $item->id }} {{ (old('user_id') ?? $teacher->User->email) == $item->id ? 'selected' : '' }}">
                                    {{ $item->email }}</option>
                            @endforeach
                        </select>
                        @error('user_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Email</label>
                        <input type="email" value="{{ old('email') ?? $teacher->email }}" name="email"
                            placeholder="email" class="form-control">

                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Địa Chỉ</label>
                        <input type="text" class="form-control @error('address') is-invalid @enderror" name="address"
                            value="{{ old('address') ?? $teacher->address }}">

                        @error('address')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Ngày Sinh</label>
                        <input type="date" value="{{ old('birth_date') ?? $teacher->birth_date }}" name="birth_date"
                            class="form-control">

                        @error('birth_date')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Trạng Thái</label>
                        <select name="status" class="form-control">
                            <option>---Chọn---</option>
                            <option value="1" {{ old('status', $teacher->status) == '1' ? 'selected' : '' }}>Có Lớp
                            </option>
                            <option value="0"{{ old('status', $teacher->status) == '0' ? 'selected' : '' }}>Không Có
                                Lớp</option>

                        </select>
                        @error('status')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Điện Thoại</label>
                        <input type="tel" value="{{ old('phone') ?? $teacher->phone }}" name="phone"
                            class="form-control">

                        @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Trình Độ English</label>
                        <select name="level" class="form-control">
                            <option>---Chọn---</option>
                            <option value="C1" {{ old('level', $teacher->level) == 'C1' ? 'selected' : '' }}>Tiếng Anh
                                Bậc C1</option>
                            <option value="C2"{{ old('level', $teacher->level) == 'C2' ? 'selected' : '' }}>Tiếng Anh
                                Bậc B2</option>
                            <option
                                value="Ielts trên 7.0"{{ old('level', $teacher->level) == 'Ielts trên 7.0' ? 'selected' : '' }}>
                                Ielts 7.0 trở lên</option>
                            <option
                                value="Toeic Trên 700"{{ old('level', $teacher->level) == 'Toeic Trên 700' ? 'selected' : '' }}>
                                Toeic 700 trở lên </option>
                        </select>
                        @error('cccd')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người tạo</label>
                        <select name="created_by" class="form-control">
                            <option>---chọn---</option>
                            <option value="admin"
                                {{ old('created_by', $teacher->created_by) == 'admin' ? 'selected' : '' }}>Admin</option>
                            <option value="staff"
                                {{ old('created_by', $teacher->created_by) == 'staff' ? 'selected' : '' }}>Staff</option>


                        </select>

                        @error('created_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người Cập Nhật</label>
                        <select name="updated_by" class="form-control">
                            <option>---chọn---</option>
                            <option value="admin"
                                {{ old('updated_by', $teacher->updated_by) == 'admin' ? 'selected' : '' }}>Admin</option>
                            <option value="staff"
                                {{ old('updated_by', $teacher->updated_by) == 'staff' ? 'selected' : '' }}>Staff</option>


                        </select>

                        @error('updated_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>

        </div>

    @endsection
