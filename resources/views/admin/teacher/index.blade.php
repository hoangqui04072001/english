@extends('admin.layout.app')
@section('title','Giáo Viên')

@section('page')
 <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Teacher</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Teacher</h6>
        </nav>
@endsection


@section('content')

<div class="card">
    @if(session('message'))
    <div> <h3 class="alert alert-success text-white">{{ session('message')}}</h3></div>
    @endif
<h1>
    Quản Lý Giáo Viên
</h1>
<div>
  <a style=" padding: 20px 25px;font-size: 20px;" href="{{route('teacher.create')}}" class="btn btn-primary">Thêm</a>
</div>
<div class="input-group input-group-outline my-3">
    <label class="form-label">Search</label>
    <input id="searchInput" type="text" class="form-control">
</div>
<div>
  <table class="table table-hover" style=" font-size: 1.4em; ">
      <tr>
          <th>#</th>
          <th>Tên</th>
          <th>Email Tài Khoản</th>
          <th>Email Cá Nhân</th>
          <th>Địa Chỉ</th>
          <th>Ngày Sinh</th>
          <th>Số Điện Thoại</th>
          <th>Trình Độ</th>
        
          <th>Action</th>
      </tr>
      @foreach ($teachers as $teacher)
          <tr>
              <td>{{$teacher ->id}}</td>
              <td>{{$teacher ->name}}</td>
              <td>{{$teacher ->User->email}}</td>
              <td>{{$teacher ->email}}</td>
              <td>{{$teacher ->address}}</td>
              <td>{{$teacher ->birth_date}}</td>
              <td>{{$teacher ->phone}}</td>
              <td>{{$teacher ->level}}</td>
             
              <td>
                  <a href="{{route('teacher.edit', $teacher->id)}}" class=" btn btn-warning btn-lg ">Edit</a>
                  
                  <form action="{{ route('teacher.destroy', $teacher->id)}}" method="post">
                  @csrf
                  @method('delete')
                      <button class=" btn btn-danger btn-lg ">Delete</button>
                  </form>
              </td>
          </tr>
      @endforeach
  </table>
  <div>
  {{ $teachers->links()}}
</div>
</div>

</div>

@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#searchInput').on('input', function() {
            var searchText = $(this).val().toLowerCase();
            $("table tbody tr").filter(function() {
                var title = $(this).find('td:eq(1)').text().toLowerCase();
                var name = $(this).find('td:eq(3)').text().toLowerCase();
                $(this).toggle(title.indexOf(searchText) > -1 || name.indexOf(searchText) > -1);
            });
        });
    });
</script>
@endsection