<aside
    class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark"
    id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
            aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href=" https://demos.creative-tim.com/material-dashboard/pages/dashboard "
            target="_blank">
            <img src="{{ asset('admin/assets/img/logo-ct.png') }}" class="navbar-brand-img h-100" alt="main_logo">
            <span class="ms-1 font-weight-bold text-white"> Quản lý khóa học</span>
        </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100" id="sidenav-collapse-main">
        <ul class="navbar-nav">
            @if (Auth::user()->hasRole('Admin'))
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('admin') ? 'active bg-gradient-primary' : '' }}"
                        href="{{ route('admin') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">dashboard</i>
                        </div>
                        <span class="nav-link-text ms-1">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('category.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('category.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">table_view</i>
                        </div>
                        <span class="nav-link-text ms-1">Category</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('product.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('product.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                inventory_2
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Product</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('videoproduct.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('videoproduct.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">view_in_ar</i>
                        </div>
                        <span class="nav-link-text ms-1">Video Product</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('class.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('class.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                school
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Class</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('discount.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('discount.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                sell
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Discount</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('calendar.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('calendar.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                calendar_month
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Calendar</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('order.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('order.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                order_approve
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Order</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('blog.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('blog.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                edit_square
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Blog</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('contact.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('contact.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                contact_phone
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Contact</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('teacher.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('teacher.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                badge
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Teacher</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('student.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('student.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                face
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Student</span>
                    </a>
                </li>

                <li class="nav-item mt-3">
                    <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Account pages</h6>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('user.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('user.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">person</i>
                        </div>
                        <span class="nav-link-text ms-1">User</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-white " href="{{ route('logout') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">login</i>
                        </div>
                        <span class="nav-link-text ms-1">Sign Out </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('role.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('role.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                manage_accounts
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Role</span>
                    </a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link text-white {{ request()->routeIs('class.index') ? 'active bg-gradient-primary' : '' }} "
                        href="{{ route('class.index') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-symbols-outlined">
                                school
                            </i>
                        </div>
                        <span class="nav-link-text ms-1">Class</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white " href="{{ route('logout') }}">
                        <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                            <i class="material-icons opacity-10">login</i>
                        </div>
                        <span class="nav-link-text ms-1">Sign Out </span>
                    </a>
                </li>
            @endif
        </ul>
    </div>

</aside>
