@extends('admin.layout.app')
@section('title', 'Liên Hệ')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Contact</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Contact</h6>
    </nav>
@endsection

@section('content')
    <div class="card">
        @if (session('message'))
            <div>
                <h3 class="alert alert-success text-white">{{ session('message') }}</h3>
            </div>
        @endif
        <h1>
            Liên Hệ
        </h1>
        <div class="input-group input-group-outline my-3">
            <label class="form-label">Search</label>
            <input id="searchInput" type="text" class="form-control">
        </div>
        <div>
            <table class="table table-hover" style=" font-size: 1.4em; ">
                <tr>
                    <th>#</th>
                    <th>tên</th>
                    <th>Email</th>
                    <th>Tiêu Đề</th>
                    <th>Nội Dung</th>
                    <th>Action</th>
                </tr>
                @foreach ($contacts as $contact)
                    <tr>
                        <td>{{ $contact->id }}</td>
                        <td>{{ $contact->name }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>{{ $contact->subject }}</td>
                        <td>{{ $contact->message }}</td>

                        <td>

                            <form action="{{ route('contact.destroy', $contact->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button class=" btn btn-danger btn-lg ">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div>
                {{ $contacts->links() }}
            </div>
        </div>

    </div>
@endsection
<script>
    $(document).ready(function() {
        $('#searchInput').on('input', function() {
            var searchText = $(this).val().toLowerCase();
            $("table tbody tr").filter(function() {
                var title = $(this).find('td:eq(1)').text().toLowerCase();
                var name = $(this).find('td:eq(3)').text().toLowerCase();
                $(this).toggle(title.indexOf(searchText) > -1 || name.indexOf(searchText) > -1);
            });
        });
    });
</script>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#searchInput').on('input', function() {
            var searchText = $(this).val().toLowerCase();
            $("table tbody tr").filter(function() {
                var title = $(this).find('td:eq(1)').text().toLowerCase();
                var name = $(this).find('td:eq(3)').text().toLowerCase();
                $(this).toggle(title.indexOf(searchText) > -1 || name.indexOf(searchText) > -1);
            });
        });
    });
</script>
@endsection

