@extends('admin.layout.app')
@section('title','Sửa Phân Quyền'.$role->name)

@section('page')
 <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Role</li>
          </ol>
          <h6 class="font-weight-bolder mb-0"> Chỉnh sửa Role</h6>
        </nav>
@endsection


@section('content')
<div class="card">

   
    <div> <h2>Chỉnh Sửa Vai Trò Người dùng</h2></div>
   
    <div>
        <form action="{{route ('role.update', $role->id)}}" method ="POST">
      @csrf
          @method('PUT')
          <div class="p-4">
          <div class="input-group input-group-static mb-4">
            <label>Name</label>
            <input type="text" value="{{old('name') ?? $role->name}}" name="name" class="form-control">
          @error('name')
            <span class="text-danger">{{$message}}</span>
          @enderror
        
          </div>

          <div class="input-group input-group-static mb-4">
            <label>Display Name</label>
            <input type="text"  value="{{old('display_name') ?? $role->display_name}}"  name="display_name" class="form-control">
          
            @error('display_name')
            <span class="text-danger">{{$message}}</span>
              
          @enderror
          </div>
        

          <div class="input-group input-group-static mb-4">
            <label class="ms-0">Group</label>
            <select name="group" class="form-control" value="{{$role->group}}">
              <option >system</option>
              <option >watch</option>
             
            </select>

            @error('group')
            <span class="text-danger">{{$message}}</span>
              
          @enderror
          </div>
          </div>
          <div class="form-group">
              <label for=""> Permission</label>
          @foreach ($permissions as $groupName => $permission)
              <div class="col-6">
                  <h4>{{$groupName}}</h4>

                  <div>
                      @foreach ($permission as $item )
                      <div class="form-check">
                          <input class="form-check-input" name="permission_id[]" type="checkbox" {{ $role->permissions->contains('name', $item->name) ? 'checked':''}} value="{{$item ->id}}">
                          <label class="custom-control-label" for="customCheck1">{{$item->display_name}}</label>
                      </div>
                          
                      @endforeach
                  </div>
              </div>
         @endforeach
        </div>
         <button type="submit" class="btn btn-submit btn-primary">Submit</button>

        </form>
    </div>
</div>

@endsection