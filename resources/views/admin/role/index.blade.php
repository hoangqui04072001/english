@extends('admin.layout.app')
@section('title','Phân Quyền')

@section('page')
 <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Role</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Role</h6>
        </nav>
@endsection


@section('content')
<div class="card">
    @if(session('message'))
    <div> <h3 class="alert alert-success text-white">{{ session('message')}}</h3></div>
    @endif
    <h1>
        Phân Quyền Người Dùng
    </h1>
    <div>
        <a style=" padding: 20px 25px;font-size: 20px;" href="{{route('role.create')}}" class="btn btn-primary">Thêm</a>
    </div>
    <div>
        <table class="table table-hover" style=" font-size: 1.4em; ">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Displayname</th>
                <th>Action</th>
            </tr>
            @foreach ($roles as $role)
                <tr>
                    <td>{{$role ->id}}</td>
                    <td>{{$role ->name}}</td>
                    <td>{{$role ->display_name}}</td>
                    <td>
                        <a href="{{route('role.edit', $role->id)}}" class=" btn btn-warning btn-lg ">Edit</a>
                        
                        <form action="{{ route('role.destroy', $role->id)}}" method="post">
                        @csrf
                        @method('delete')
                            <button class=" btn btn-danger btn-lg ">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        <div>
        {{ $roles->links()}}
    </div>
    </div>

</div>
@endsection