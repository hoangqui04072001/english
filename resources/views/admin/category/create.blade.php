@extends('admin.layout.app')
@section('title', 'Tạo Loại Khóa Học')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Catogary</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Create Catogary</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Tạo Khóa Học</h2>
        </div>

        <div>
            <form action="{{ route('category.store') }}" method="POST">
                @csrf
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên Khóa Học</label>
                        <input type="text" value="{{ old('name') }}" name="name" class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Slug Tên Khóa Học</label>
                        <input type="text" value="{{ old('name_slug') }}" name="name_slug" class="form-control"
                            placeholder="name-name">
                        @error('name_slug')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Trạng Thái</label>
                        <select name="status" class="form-control">
                            <option value="1">Active</option>
                            <option value="0">Close</option>


                        </select>

                        @error('status')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>


                    <div class="input-group input-group-static mb-4">
                        <label>Description</label>
                        <input type="text" value="{{ old('description') }}" name="description" placeholder="Mô tả"
                            class="form-control">

                        @error('description')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người Tạo</label>
                        <select name="created_by" class="form-control">
                            <option>Admin</option>
                            <option>Staff</option>
                            <option>Other</option>

                        </select>

                        @error('created_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người Cập Nhật</label>
                        <select name="update_by" class="form-control">
                            <option>Admin</option>
                            <option>Staff</option>
                            <option>Other</option>

                        </select>

                        @error('update_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>
    </div>

@endsection
