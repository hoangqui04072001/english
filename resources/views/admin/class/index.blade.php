@extends('admin.layout.app')
@section('title', 'Lớp Học')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Class</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Class</h6>
    </nav>
@endsection


@section('content')
    <div class="card">
        @if (session('message'))
            <div>
                <h3 class="alert alert-success text-white">{{ session('message') }}</h3>
            </div>
        @endif
        <h1>
            Lớp Học
        </h1>
        @if (Auth::user()->hasRole('Admin'))
            <div>
                <a style=" padding: 20px 25px; font-size: 20px;" href="{{ route('class.create') }}"
                    class="btn btn-primary">Thêm</a>
            </div>
        @endif
        <div class="input-group input-group-outline my-3">
            <label class="form-label">Search</label>
            <input id="searchInput" type="text" class="form-control">
        </div>
        <div>
            <table class="table table-hover" style=" font-size: 1.4em; ">
                <tr>
                    <th>#</th>
                    <th>Tên lớp học</th>
                    <th>Mã lớp</th>
                    <th>Trạng thái</th>
                    <th>Mô tả</th>
                    <th>Số buổi học</th>
                    <th>Lịch Học</th>
                    <th>Giáo viên</th>
                    <th>Khóa học</th>
                    <th>Số lượng học viên</th>
                    <th>Thời gian bắt đầu</th>
                    <th>Thời gian kết thúc</th>
                    @if (Auth::user()->hasRole('Admin'))
                        <th>Action</th>
                    @endif
                </tr>
                @foreach ($classes as $class)
                    <tr>
                        <td>{{ $class->id }}</td>
                        <td>{{ $class->name }}</td>
                        <td>{{ $class->code }}</td>
                        <td>{{ $class->status }}</td>
                        <td>{{ $class->description }}</td>
                        <td>{{ $class->sessions_number }}</td>
                        <td>{{ $class->Calendar->lesson }}</td>
                        <td>{{ $class->Teacher->name }}</td>
                        <td>{{ $class->Product->name }}</td>
                        <td>{{ $class->quantity }}</td>
                        <td>{{ $class->start_date }}</td>
                        <td>{{ $class->end_date }}</td>
                        <td>
                            @if (Auth::user()->hasRole('Admin'))
                                <a href="{{ route('class.edit', $class->id) }}" class=" btn btn-warning btn-lg ">Edit</a>

                                <form action="{{ route('class.destroy', $class->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class=" btn btn-danger btn-lg ">Delete</button>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
            <div>
                {{ $classes->links() }}
            </div>
        </div>

    </div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#searchInput').on('input', function() {
            var searchText = $(this).val().toLowerCase();
            $("table tbody tr").filter(function() {
                var title = $(this).find('td:eq(1)').text().toLowerCase();
                var name = $(this).find('td:eq(3)').text().toLowerCase();
                $(this).toggle(title.indexOf(searchText) > -1 || name.indexOf(searchText) > -1);
            });
        });
    });
</script>
@endsection
