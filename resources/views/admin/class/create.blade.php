@extends('admin.layout.app')
@section('title', 'Tạo Lớp')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Catogary</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Create Class</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Tạo Lớp</h2>
        </div>

        <div>
            <form action="{{ route('class.store') }}" method="POST">
                @csrf
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên Lớp</label>
                        <input type="text" value="{{ old('name') }}" name="name" class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Mã Lớp</label>
                        <input type="text" value="{{ old('code') }}" name="code" class="form-control"
                            placeholder="P-Number">
                        @error('code')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Trạng Thái</label>
                        <select name="status" class="form-control">
                            <option>---Chọn---</option>
                            <option value="Active">Active</option>
                            <option value="Close">Close</option>


                        </select>

                        @error('status')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>


                    <div class="input-group input-group-static mb-4">
                        <label>Mô tả</label>
                        <input type="text" value="{{ old('description') }}" name="description" placeholder="Mô tả"
                            class="form-control">

                        @error('description')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>


                    <div class="input-group input-group-static mb-4">
                        <label>Số tiết học</label>
                        <input type="phone" value="{{ old('sessions_number') }}" name="sessions_number"
                            class="form-control">

                        @error('sessions_number')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Lịch Học</label>
                        <select type="text" name="calendar_id" class="form-control">
                            <option>---chọn----</option>
                            @foreach ($calendar as $item)
                                <option value="{{ $item->id }}">{{ $item->lesson }}</option>
                            @endforeach
                        </select>
                        @error('calendar_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Giáo Viên</label>
                        <select type="text" name="teacher_id" class="form-control">
                            <option>---chọn----</option>
                            @foreach ($teacher as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('teacher_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Khóa Học</label>
                        <select type="text" name="product_id" class="form-control">
                            <option>---chọn----</option>
                            @foreach ($product as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('product_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Số Lượng Học Viên</label>
                        <input type="phone" value="{{ old('quantity') }}" name="quantity" placeholder=""
                            class="form-control">

                        @error('quantity')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Ngày bắt đầu</label>
                        <input type="date" name="start_date" placeholder="" class="form-control">

                        @error('start_date')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Ngày Kết Thúc</label>
                        <input type="date" name="end_date" placeholder="" class="form-control">

                        @error('end_date')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>


                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người Tạo</label>
                        <select name="created_by" class="form-control">
                            <option>---Chọn---</option>
                            <option value="Admin">Admin</option>
                            <option value="Staff">Staff</option>
                            <option value="Other">Other</option>

                        </select>

                        @error('created_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người Cập Nhật</label>
                        <select name="updated_by" class="form-control">
                            <option>---Chọn---</option>
                            <option value="Admin">Admin</option>
                            <option value="Staff">Staff</option>
                            <option value="Other">Other</option>

                        </select>

                        @error('update_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>
    </div>

@endsection
