@extends('admin.layout.app')
@section('title', 'Sửa Lịch Học' . $calendar->lesson)

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Calendar</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Edit Calendar</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Sửa Lịch Học</h2>
        </div>

        <div>
            <form action="{{ route('calendar.update', $calendar->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên Lịch Học</label>
                        <input type="text" value="{{ old('lesson') ?? $calendar->lesson }}" name="lesson"
                            class="form-control">
                        @error('lesson')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Mô Tả Lịch Học</label>
                        <input type="text" value="{{ old('description') ?? $calendar->description }}" name="description"
                            class="form-control">
                        @error('description')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>


                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>
    </div>
@endsection
