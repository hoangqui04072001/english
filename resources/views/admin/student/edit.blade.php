@extends('admin.layout.app')
@section('title', 'Học Viên ' . $student->name)

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Student</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Student</h6>
    </nav>
@endsection


@section('content')

    <div class="card">
        @if (session('message'))
            <div>
                <h3 class="alert alert-success text-white">{{ session('message') }}</h3>
            </div>
        @endif
        <h1>
            Tạo Học Viên
        </h1>
        <div>
            <form action="{{ route('student.update', $student->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên</label>
                        <input type="text" value="{{ old('name') ?? $student->name }}" name="name"
                            class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Mã Học Viên</label>
                        <input type="text" value="{{ old('code') ?? $student->code }}" name="code" placeholder=""
                            class="form-control">

                        @error('code')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Điện Thoại</label>
                        <input type="tel" value="{{ old('phone') ?? $student->phone }}" name="phone"
                            class="form-control">

                        @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Email Tài Khoản</label>
                        <select type="text" name="user_id" class="form-control">
                            @foreach ($useremail as $item)
                                <option
                                    value="{{ $item->id }} {{ (old('user_id') ?? $student->User->email) == $item->id ? 'selected' : '' }}">
                                    {{ $item->email }}</option>
                            @endforeach
                        </select>
                        @error('user_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Email Cá Nhân</label>
                        <input type="email" value="{{ old('email') ?? $student->email }}" name="email"
                            class="form-control">

                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Ngày Sinh</label>
                        <input type="date" value="{{ old('birth_date') ?? $student->birth_date }}" name="birth_date"
                            class="form-control">

                        @error('birth_date')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Gender</label>
                        <select name="gender" class="form-control">
                            <option>---chọn---</option>
                            <option value="Nam" {{ old('gender', $student->gender) == 'Nam' ? 'selected' : '' }}>Nam
                            </option>
                            <option value="Nữ" {{ old('gender', $student->gender) == 'Nữ' ? 'selected' : '' }}>Nữ
                            </option>
                            <option value="Khác" {{ old('gender', $student->gender) == 'Khác' ? 'selected' : '' }}>Khác
                            </option>

                        </select>

                        @error('gender')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người tạo</label>
                        <select name="created_by" class="form-control">
                            <option>---chọn---</option>
                            <option value="admin"
                                {{ old('created_by', $student->created_by) == 'admin' ? 'selected' : '' }}>Admin</option>
                            <option value="staff"
                                {{ old('created_by', $student->created_by) == 'staff' ? 'selected' : '' }}>Staff</option>


                        </select>

                        @error('created_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người Cập Nhật</label>
                        <select name="updated_by" class="form-control"
                            value="{{ old('updated_by') ?? $student->updated_by }}">
                            <option>---chọn---</option>
                            <option value="admin"
                                {{ old('updated_by', $student->updated_by) == 'admin' ? 'selected' : '' }}>Admin</option>
                            <option value="staff"
                                {{ old('updated_by', $student->updated_by) == 'staff' ? 'selected' : '' }}>Staff</option>


                        </select>

                        @error('updated_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>


    @endsection
