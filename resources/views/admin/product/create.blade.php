@extends('admin.layout.app')
@section('title', 'Tạo Khóa Học')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Catogary</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Create Catogary</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Tạo Khóa Học</h2>
        </div>

        <div>
            <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên Khóa Học</label>
                        <input type="text" value="{{ old('name') }}" name="name" class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Tên Loại Khóa Học</label>
                        <select type="text" name="category_id" class="form-control">
                            @foreach ($category as $item)
                                <option value="">---chọn---</option>
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('category_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Tên Giáo viên</label>
                        <select type="text" name="teacher_id" class="form-control">
                                <option >---chọn---</option>
                            @foreach($teacher as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('teacher_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    
                    <div class="input-group input-group-static mb-4">
                        <label>Mô Tả</label>
                        <input type="text" value="{{ old('description') }}" name="description" class="form-control">
                        @error('description')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label>Giá Bán</label>
                        <input type="number" min="0" id="sell" value="{{ old('sell') }}" name="sell"
                            class="form-control">
                        @error('sell')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label>Số Lượng</label>
                        <input type="phone" min="0" id="number" value="{{ old('number') }}" name="number"
                            class="form-control">
                        @error('number')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>
                    <div class="row">
                        <div class="input-group-static col-5 mb-4">
                            <label>Hình Ảnh</label>
                            <input type="file" accept="image/*" input="image-input" name="picture" id="picture"
                                class="form-control">
                            @error('picture')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror

                        </div>
                        <div class="col-5">
                            <img src="" id="show-image" width="200" height="200">
                        </div>
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người Tạo</label>
                        <select name="created_by" class="form-control">
                            <option value="">---Chọn---</option>
                            <option value="0">Admin</option>
                            <option value="1">Staff</option>
                            <option value="2">Other</option>

                        </select>

                        @error('created_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label class="ms-0">Người Cập Nhật</label>
                        <select name="updated_by" class="form-control">
                            <option value="">---Chọn---</option>
                            <option value="0">Admin</option>
                            <option value="1">Staff</option>
                            <option value="2">Other</option>

                        </select>

                        @error('updated_by')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>
    </div>


@endsection

@section('script')
    <script src="https://code.jquery.com/jquery-3.6.2.min.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#show-image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            $('input[name="picture"]').change(function() {
                readURL(this);
            });
        });
    </script>
@endsection
