@extends('admin.layout.app')
@section('title', 'Video Khóa Học ' . $product->name)

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Product Show</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Video Hiển thị</h6>
    </nav>
@endsection

@section('content')
    <div class="card">
        <h1>
            Video Khóa Học
        </h1>

        @foreach ($product->VideoProduct as $item)
            <td>
                <div class="card">
                    <div class="card-body">
                        <h4>{{ $item->name }}</h4>
                        <h4><video width="320" height="240" controls>
                                <source src="{{ asset('upload/' . $item->video) }}" type="video/mp4">
                            </video></h4>
                    </div>
                </div>
            </td>
        @endforeach
        <div>
            {{ $videos->links() }}
        </div>
    </div>


@endsection
