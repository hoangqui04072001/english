@extends('admin.layout.app')
@section('title', 'Khóa Học')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Product</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Product</h6>
    </nav>
@endsection

@section('content')
    <div class="card">
        @if (session('message'))
            <div>
                <h3 class="alert alert-success text-white">{{ session('message') }}</h3>
            </div>
        @endif
        <h1>
            Khóa Học
        </h1>
        <div>
            <a style=" padding: 20px 25px;font-size: 20px;" href="{{ route('product.create') }}"
                class="btn btn-primary">Thêm</a>
        </div>
        <div class="input-group input-group-outline my-3">
            <label class="form-label">Search</label>
            <input id="searchInput" type="text" class="form-control">
        </div>
        <div>
            <table class="table table-hover" style=" font-size: 1.4em; ">
                <tr>
                    <th>#</th>
                    <th>Tên Khóa Học</th>
                    <th>Loại Khóa Học</th>
                    <th>Giáo Viên</th>
                    <th>Giá bán</th>
                    <th>số lượng</th>
                    <th>Hình Ảnh</th>
                    <th>Action</th>
                </tr>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->Category->name }}</td>
                        <td>{{ $product->Teacher->name }}</td>
                        <td>{{ number_format($product->sell) }}</td>
                        <td>{{ $product->number }}</td>
                        <td><img src="{{ asset($product->picture) }}" width="80" height="80"></td>

                        <td>
                            <a href="{{ route('product.edit', $product->id) }}" class=" btn btn-warning btn-lg ">Edit</a>
                            <a href="{{ route('product.show', $product->id) }}" class="btn btn-info btn-lg">Xem video</a>
                            <form action="{{ route('product.destroy', $product->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button class=" btn btn-danger btn-lg ">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div>
                {{ $products->links() }}
            </div>
        </div>

    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#searchInput').on('input', function() {
                var searchText = $(this).val().toLowerCase();
                $("table tbody tr").filter(function() {
                    var title = $(this).find('td:eq(1)').text().toLowerCase();
                    var name = $(this).find('td:eq(3)').text().toLowerCase();
                    $(this).toggle(title.indexOf(searchText) > -1 || name.indexOf(searchText) > -1);
                });
            });
        });
    </script>
@endsection
