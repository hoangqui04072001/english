@extends('admin.layout.app')
@section('title', '')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Order</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Order</h6>
    </nav>
@endsection


@section('content')

    <div class="card">
        <h1>
            Đơn hàng
        </h1>
        <div class="input-group input-group-outline my-3">
            <label class="form-label">Search</label>
            <input id="searchInput" type="text" class="form-control">
        </div>
        <div>
            <table class="table table-hover" style=" font-size: 1.4em; ">
                <tr>
                    <th>#</th>
                    <th>Mã đơn</th>
                    <th>Họ tên</th>
                    <th>Số điện thoại</th>
                    <th>Email</th>
                    <th>Giới tính</th>
                    <th>Thời gian đặt</th>
                    <th>Trạng thái</th>
                    <th>Chức Năng</th>
                </tr>
                @foreach ($orders as $key => $order)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->order_name }}</td>
                        <td>{{ $order->order_phone }}</td>
                        <td>{{ $order->order_email }}</td>
                        <td>{{ $order->order_gender == 1 ? 'Nam' : ($order->order_gender == 2 ? 'Nữ' : 'Khác') }}</td>
                        <td>{{ date('d/m/Y H:i:s', strtotime($order->created_at)) }}</td>
                        <td>{{ $order->status == 1 ? 'Đã thanh toán' : 'Chưa thanh toán' }}</td>
                        <td>
                            <a href="{{ route('order.show', ['order' => $order->id]) }}" class="btn btn-warning btn-sm">Chi
                                tiết</a>

                            <form action="{{ route('order.destroy', ['order' => $order->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                <button class=" btn btn-danger btn-lg ">Xóa</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div>
                {{ $orders->links() }}
            </div>
        </div>

    </div>

@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#searchInput').on('input', function() {
            var searchText = $(this).val().toLowerCase();
            $("table tbody tr").filter(function() {
                var title = $(this).find('td:eq(1)').text().toLowerCase();
                var name = $(this).find('td:eq(3)').text().toLowerCase();
                $(this).toggle(title.indexOf(searchText) > -1 || name.indexOf(searchText) > -1);
            });
        });
    });
</script>
@endsection