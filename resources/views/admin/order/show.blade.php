@extends('admin.layout.app')
@section('title', '')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Order</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Order</h6>
    </nav>
@endsection


@section('content')

    <div class="card">
        <h1>
            Chi tiết đơn hàng
        </h1>
        <div>
            <table class="table table-hover" style=" font-size: 1.4em; ">
                <tr>
                    <th>STT</th>
                    <th>Khóa học</th>
                    <th>Số lượng</th>
                    <th>Mệnh giá</th>
                </tr>
                @foreach ($orderDetails as $key => $orderDetail)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ \App\Models\Product::find($orderDetail->product_id)->name }}</td>
                        <td>{{ $orderDetail->sellnumber }}</td>
                        <td>{{ number_format($orderDetail->sell) }} VNĐ</td>
                    </tr>
                @endforeach
            </table>
            <div>
                {{ $orderDetails->links() }}
            </div>
        </div>

    </div>

@endsection
