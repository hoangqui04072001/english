@extends('admin.layout.app')
@section('title', 'Giá ban đầu')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Discount</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Discount</h6>
    </nav>
@endsection

@section('content')
    <div class="card">
        @if (session('message'))
            <div>
                <h3 class="alert alert-success text-white">{{ session('message') }}</h3>
            </div>
        @endif
        <h1>
            Giá
        </h1>
        <div>
            <a style=" padding: 20px 25px; font-size: 20px;" href="{{ route('discount.create') }}"
                class="btn btn-primary">Thêm</a>
        </div>
        <div class="input-group input-group-outline my-3">
            <label class="form-label">Search</label>
            <input id="searchInput" type="text" class="form-control">
        </div>
        <div>
            <table class="table table-hover" style=" font-size: 1.4em; ">
                <tr>
                    <th>#</th>
                    <th>Khóa Học</th>
                    <th>Giá Cũ</th>
                    <th>Action</th>
                </tr>
                @foreach ($discounts as $discount)
                    <tr>
                        <td>{{ $discount->id }}</td>
                        <td>{{ $discount->Product->name }}</td>
                        <td>{{ $discount->discount }}</td>


                        <td>
                            <a href="{{ route('discount.edit', $discount->id) }}" class=" btn btn-warning btn-lg ">Edit</a>

                            <form action="{{ route('discount.destroy', $discount->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button class=" btn btn-danger btn-lg ">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div>
                {{ $discounts->links() }}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#searchInput').on('input', function() {
                var searchText = $(this).val().toLowerCase();
                $("table tbody tr").filter(function() {
                    var title = $(this).find('td:eq(1)').text().toLowerCase();
                    var name = $(this).find('td:eq(3)').text().toLowerCase();
                    $(this).toggle(title.indexOf(searchText) > -1 || name.indexOf(searchText) > -1);
                });
            });
        });
    </script>
@endsection
