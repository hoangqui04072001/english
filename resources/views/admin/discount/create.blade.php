@extends('admin.layout.app')
@section('title', 'Tạo giá')

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Discount</li>
        </ol>
        <h6 class="font-weight-bolder mb-0">Create Discount</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Tạo Giá</h2>
        </div>

        <div>
            <form action="{{ route('discount.store') }}" method="POST">
                @csrf
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên Khóa Học</label>
                        <select type="text" name="product_id" class="form-control">
                            <option>---chọn---</option>
                            @foreach ($product as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('product_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label>Giá Cũ</label>
                        <input type="text" value="{{ old('discount') }}" name="discount" class="form-control">
                        @error('discount')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>


                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>
    </div>
@endsection
