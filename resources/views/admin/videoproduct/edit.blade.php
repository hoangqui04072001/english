@extends('admin.layout.app')
@section('title', 'Tạo Video' . $video->name)

@section('page')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Video</li>
        </ol>
        <h6 class="font-weight-bolder mb-0"> Sửa Video</h6>
    </nav>
@endsection


@section('content')
    <div class="card">


        <div>
            <h2>Sửa video</h2>
        </div>

        <div>
            <form action="{{ route('videoproduct.update', $video->id) }}" enctype="multipart/form-data" method="POST">
                @csrf
                @method('PUT')
                <div class="p-4">
                    <div class="input-group input-group-static mb-4">
                        <label>Tên Video</label>
                        <input type="text" value="{{ old('name', $video->name) }}" name="name" class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Video</label>
                        <input type="file" name="video" class="form-control">
                        @if ($video->video)
                            <div class="mt-2"><b>Video hiện tại:</b> {{ $video->video }}</div>
                        @endif
                        @error('video')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Thuộc Khóa Học</label>
                        <select type="text" name="products_id" class="form-control">
                            <option value="">---Chọn---</option>
                            @foreach ($product as $item)
                                <option value="{{ $item->id }}"
                                    {{ old('products_id', $video->products_id) == $item->id ? 'selected' : '' }}>
                                    {{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('product_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-submit btn-primary">Submit</button>

            </form>
        </div>
    </div>

@endsection
