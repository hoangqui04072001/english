<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('client/img/fav.png') }}">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Blog</title>
    <style>
        #searchResults {
            height: 100px;
            overflow-y: auto;
        }
    </style>
    <style>
        .post-list {
            list-style-type: none;
            padding: 0;
            margin: 0;
            max-height: 300px;
            overflow-y: auto;

        }
    </style>
    <!--
   CSS
   ============================================= -->
    <link rel="stylesheet" href="{{ asset('client/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/main.css') }}">
</head>

<body>

    <!-- Start Header Area -->
    @include('client.layout.header')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Blog </h1>
                    <nav class="d-flex align-items-center">
                        <a href="{{ route('home') }}">Trang Chủ<span class="lnr lnr-arrow-right"></span></a>
                        <a href="{{ route('client.blog') }}">Blog</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Blog Categorie Area =================-->
    <section class="blog_categorie_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="categories_post">
                        <img src="https://engbreaking.com/wp-content/uploads/2023/03/chung-chi-tieng-Anh.jpg"
                            alt="post">
                        <div class="categories_details">
                            <div class="categories_text">
                                <a href="">
                                    <h5>Tiếng Anh Mỗi Ngày</h5>
                                </a>
                                <div class="border_line"></div>
                                <p>Tham gia các khóa học tiếng anh ngay thôi !!!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="categories_post">
                        <img src="https://engbreaking.com/wp-content/uploads/2021/11/2000x682_A_cac-con-vat-TA-1400x440.jpg"
                            alt="post">
                        <div class="categories_details">
                            <div class="categories_text">
                                <a href="">
                                    <h5>Làm Các Bài Tập Tiếng Anh</h5>
                                </a>
                                <div class="border_line"></div>
                                <p>các bộ đề kiểm tra mẫu</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="categories_post">
                        <img src="https://engbreaking.com/wp-content/uploads/2023/03/quy-tac-trong-am-1.png"
                            alt="post">
                        <div class="categories_details">
                            <div class="categories_text">
                                <a href="">
                                    <h5>1000 Từ Vựng Mỗi Ngày</h5>
                                </a>
                                <div class="border_line"></div>
                                <p>Cùng học mỗi ngày nào</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Categorie Area =================-->

    <!--================Blog Area =================-->
    <section class="blog_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog_left_sidebar">
                        @foreach ($blogs as $blog)
                            <article class="row blog_item">
                                <div class="col-md-3">
                                    <div class="blog_info text-right">
                                        <div class="post_tag">
                                            <a class="active" href="#">{{ $blog->categoryblog }}</a>
                                        </div>
                                        <ul class="blog_meta list">
                                            <li><a href="#">{{ $blog->name }}<i class="lnr lnr-user"></i></a>
                                            </li>
                                            <li><a href="#">{{ $blog->created_at }}<i
                                                        class="lnr lnr-calendar-full"></i></a>
                                            </li>
                                            <li><a href="#">{{ $blog->views }}Views<i
                                                        class="lnr lnr-eye"></i></a></li>
                                            <li><a href="#">06 Comments<i class="lnr lnr-bubble"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="blog_post">
                                        <img src="{{ $blog->image }}" alt="">
                                        <div class="blog_details">
                                            <a href="single-blog.html">
                                                <h2>{{ $blog->title }}</h2>
                                            </a>

                                            <a href="{{ route('client.blog.details', ['id' => $blog->id]) }}"
                                                class="white_bg_btn">Xem Chi Tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        @endforeach
                        {{-- <div>{{ $blogs->links() }}</div> --}}
                        <nav class="blog-pagination justify-content-center d-flex">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a href="{{ $blogs->previousPageUrl() }}" class="page-link" aria-label="Previous">
                                        <span aria-hidden="true">
                                            <span class="lnr lnr-chevron-left"></span>
                                        </span>
                                    </a>
                                </li>
                                @foreach ($blogs->getUrlRange($blogs->currentPage() + 0, $blogs->currentPage() + 2) as $page => $url)
                                    <li class="page-item {{ $page == $blogs->currentPage() ? 'active' : '' }}">
                                        <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                                    </li>
                                @endforeach
                                <li class="page-item">
                                    <a href="{{ $blogs->nextPageUrl() }}" class="page-link" aria-label="Next">
                                        <span aria-hidden="true">
                                            <span class="lnr lnr-chevron-right"></span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget search_widget">
                            <form id="searchForm" action="{{ route('client.blog.sreach') }}" method="POST">
                                @csrf
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search Blog"
                                        name="searchInput" id="searchInput" onfocus="this.placeholder = ''"
                                        onblur="this.placeholder = 'Search Blog'">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i
                                                class="lnr lnr-magnifier"></i></button>
                                    </span>
                                </div>
                            </form>
                            <div id="searchResults"></div>
                            <!-- Hiển thị kết quả tìm kiếm trong file view của bạn -->


                        </aside>

                        <aside class="single_sidebar_widget popular_post_widget">
                            <h3 class="widget_title">Bài Viết Phổ Biến</h3>
                            <ul class="post-list">
                                @foreach ($blogs as $blog)
                                    <div class="media post_item">
                                        <img src="{{ $blog->image }}" height="50px" width="50px"
                                            alt="post">
                                        <div class="media-body">
                                            <a href="blog-details.html">
                                                <h3>{{ $blog->title }}</h3>
                                            </a>
                                            <p>{{ $blog->created_at }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </ul>
                            <div class="br"></div>
                        </aside>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->

    <!-- start footer Area -->
    @include('client.layout.footer')
    <!-- End footer Area -->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/643ca64131ebfa0fe7f8aa31/1gu6eisib';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('client/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('client/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('client/js/nouislider.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{ asset('client/js/gmaps.min.js') }}"></script>
    <script src="{{ asset('client/js/main.js') }}"></script>
    <!-- Thêm đoạn mã JavaScript sau vào trang HTML của bạn -->

    <script type="text/javascript">
        document.getElementById('searchForm').addEventListener('submit', function(event) {
            event.preventDefault(); // Ngăn chặn việc submit form
            var input = document.getElementById('searchInput').value;
            var resultsDiv = document.getElementById('searchResults');

            // Gửi yêu cầu tìm kiếm đến server
            fetch('{{ route('client.blog.sreach') }}', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute(
                            'content')
                    },
                    body: JSON.stringify({
                        searchInput: input
                    })
                })
                .then(response => response.json())
                .then(data => {
                    // Xóa kết quả tìm kiếm trước đó
                    resultsDiv.innerHTML = '';

                    // Hiển thị kết quả tìm kiếm mới
                    data.forEach(result => {
                        var resultDiv = document.createElement('div');
                        resultDiv.textContent = result.title;
                        resultsDiv.appendChild(resultDiv);
                    });
                })
                .then(results => {
                    // Kiểm tra kết quả tìm kiếm
                    if (results.length > 0) {
                        // Nếu có kết quả, hiện div #searchResults
                        document.getElementById('searchResults').style.display = 'block';
                        // Xử lý kết quả tìm kiếm, ví dụ: append dữ liệu vào div #searchResults
                        // ...
                    } else {
                        // Nếu không có kết quả, ẩn div #searchResults
                        document.getElementById('searchResults').style.display = 'none';
                    }
                })

                .catch(error => console.error(error));
        });
    </script>
</body>

</html>
