<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('client/img/fav.png') }}">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Detail Blog</title>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <!--
   CSS
   ============================================= -->
    <link rel="stylesheet" href="{{ asset('client/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/ion.rangeSlider.skinFlat.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/main.css') }}">

    <style>
        .post-list {
            list-style-type: none;
            padding: 0;
            margin: 0;
            max-height: 300px;
            /* Thiết lập chiều cao tối đa của thanh trượt */
            overflow-y: auto;
            /* Thiết lập overflow-y để có thanh trượt dọc */
        }
    </style>
</head>

<body>

    <!-- Start Header Area -->
    @include('client.layout.header')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Blog </h1>
                    <nav class="d-flex align-items-center">
                        <a href="{{ route('home') }}">Trang Chủ<span class="lnr lnr-arrow-right"></span></a>
                        <a href="{{ route('client.blog') }}">Detail Blog</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Blog Area =================-->
    <section class="blog_area single-post-area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 posts-list">

                    <div class="single-post row">
                        <div class="col-lg-12">
                            <div class="feature-img">
                                <img class="img-fluid" src="{{ $blog->image }}" alt="">
                            </div>
                        </div>
                        <div class="col-lg-3  col-md-3">
                            <div class="blog_info text-right">
                                <div class="post_tag">

                                    <a class="active" href="#">{{ $blog->categoryblog }}</a>

                                </div>
                                <ul class="blog_meta list">
                                    <li><a href="#">{{ $blog->name }}<i class="lnr lnr-user"></i></a></li>
                                    <li><a href="#">{{ $blog->created_at }}<i
                                                class="lnr lnr-calendar-full"></i></a>
                                    </li>
                                    <li><a href="#">{{ $blog->views }}<i class="lnr lnr-eye"></i></a></li>
                                    <li><a href="#">06 Comments<i class="lnr lnr-bubble"></i></a></li>
                                </ul>
                                <ul class="social-links">
                                    <li><a href="https://www.facebook.com/profile.php?id=100008963505174"><i
                                                class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://gitlab.com/hoangqui04072001/english"><i
                                                class="fa fa-github"></i></a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 blog_details">
                            <h2>{{ $blog->title }}</h2><br>
                            <div style="max-height: 1500px; overflow-y: auto;">
                                <p>{!! htmlspecialchars_decode($textareaContent) !!}</p>
                            </div>
                        </div>

                    </div>



                </div>
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget search_widget">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search Posts"
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Posts'">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i
                                            class="lnr lnr-magnifier"></i></button>
                                </span>
                            </div><!-- /input-group -->
                            <div class="br"></div>
                        </aside>
                        <aside class="single_sidebar_widget author_widget">
                            <img class="author_img rounded-circle"
                                src="https://cdn-icons-png.flaticon.com/512/145/145867.png" height="60px"
                                width="60px" alt="">
                            <h4>{{ $blog->name }}</h4>
                            <p>Người viết bài này</p>
                            <div class="social_icon">
                                <li><a href="https://www.facebook.com/profile.php?id=100008963505174"><i
                                            class="fa fa-facebook"></i></a></li>
                                <li><a href="https://gitlab.com/hoangqui04072001/english"><i
                                            class="fa fa-github"></i></a></li>
                            </div>
                            <p>với tiêu chí đem sự hiểu biết cho mọi người cùng học tiếng anh</p>
                            <div class="br"></div>
                        </aside>
                        <aside class="single_sidebar_widget popular_post_widget">
                            <h3 class="widget_title">Các bài viết khác</h3>
                            <ul class="post-list">
                                @foreach ($blogs as $item)
                                    <div class="media post_item">
                                        <img src="{{ $item->image }}" width="100px" height="50px"
                                            alt="post">
                                        <div class="media-body">
                                            <a href="blog-details.html">
                                                <h3>{{ $item->title }}</h3>
                                            </a>
                                            <p>{{ $item->created_at }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </ul>
                            <div class="br"></div>
                        </aside>
                        <aside class="single_sidebar_widget ads_widget">
                            <a href="#"><img class="img-fluid"
                                    src="https://engbreaking.com/wp-content/uploads/2023/04/EBVietj.banner03-1.png"
                                    alt=""></a>
                            <div class="br"></div>
                        </aside>
                        <aside class="single_sidebar_widget post_category_widget">
                            <h4 class="widget_title">Các loại danh mu</h4>
                            <ul class="list cat-list">
                                <li>
                                    <a href="#" class="d-flex justify-content-between">
                                        <p>1000 Từ Vựng Mỗi Ngày</p>

                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="d-flex justify-content-between">
                                        <p>Tiếng Anh Mỗi Ngày</p>

                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="d-flex justify-content-between">
                                        <p>Làm Các Bài Tập Tiếng Anh</p>

                                    </a>
                                </li>
                                <li>

                            </ul>
                            <div class="br"></div>
                        </aside>
                        <aside class="single-sidebar-widget newsletter_widget">
                            <h4 class="widget_title">Liên Hệ</h4>
                            <p>
                                Ở đây, nếu bạn có cần thì gửi email liên hệ chúng tôi.
                            </p>
                            <div class="form-group d-flex flex-row">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-envelope"
                                                aria-hidden="true"></i></div>
                                    </div>
                                    <input type="text" class="form-control" id="inlineFormInputGroup"
                                        placeholder="Enter email" onfocus="this.placeholder = ''"
                                        onblur="this.placeholder = 'Enter email'">
                                </div>
                                <a href="#" class="bbtns">Gửi</a>
                            </div>

                            <div class="br"></div>
                        </aside>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->

    <!-- start footer Area -->
    @include('client.layout.footer')

    <!-- End footer Area -->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/643ca64131ebfa0fe7f8aa31/1gu6eisib';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('client/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('client/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('client/js/nouislider.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{ asset('client/js/gmaps.min.js') }}"></script>
    <script src="{{ asset('client/js/main.js') }}"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>


    <script>
        // Lấy giá trị của textarea và đưa vào trong div có id là descriptionContainer
        var blogDescription = document.getElementById('blogDescription').value;
        document.getElementById('descriptionContainer').innerHTML = blogDescription;
    </script>
    <script>
        var swiper = new Swiper('.popular_post_widget', {
            // Cấu hình Swiper
            // Ví dụ: 
            slidesPerView: 1, // Số lượng slide hiển thị trên mỗi lần trượt
            spaceBetween: 2, // Khoảng cách giữa các slide
            navigation: {
                nextEl: '.swiper-button-next', // Selector của nút điều hướng slide tiếp theo
                prevEl: '.swiper-button-prev', // Selector của nút điều hướng slide trước đó
            }
        });
    </script>
</body>

</html>
