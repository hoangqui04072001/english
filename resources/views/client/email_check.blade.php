<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('client/img/fav.png') }}">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Đơn Hàng Thành Công</title>
    <style>
        .title_confirmation {
            text-align: center;
            /* căn giữa đoạn văn bản */
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 10px;
            /* căn giữa hình ảnh */
        }

        .title_confirmation img {
            width: 60px;
            height: 60px;


            display: block;
            /* chuyển hình ảnh thành phần tử block */
            margin: 0 auto;
            /* căn giữa hình ảnh trong phần tử cha */


        }

        .login_box_img {
            position: relative;
            /* Đặt phần tử vị trí tuyệt đối */
            top: 50%;
            /* Đặt vị trí đỉnh của phần tử ở giữa theo chiều dọc */
            left: 50%;
            /* Đặt vị trí bên trái của phần tử ở giữa theo chiều ngang */
            transform: translate(-50%, -50%);
            display: flex;
            /* Áp dụng thuộc tính display: flex; */
            justify-content: center;
            /* Căn giữa theo chiều ngang */
            align-items: center;

        }

        .title_confirmation span {
            margin-top: 10px;

        }
    </style>

    <!--
  CSS
  ============================================= -->
    <link rel="stylesheet" href="{{ asset('client/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/main.css') }}">
</head>

<body>


    <!--================Order Details Area =================-->
    <section class="order_details section_gap">
        <div class="container">
            <h3 class="title_confirmation">Hi {{ $order->order_name }}, Cảm ơn bạn đã tin tưởng mua hàng chúng tôi !!!
            </h3>
            <p> Chúng tôi đã gửi
                khóa học qua cho bạn nó nằm trong phần thông tin nhé. Hãy kiểm tra nó.
            <table style="border-collapse: collapse; width: 100%;">
                <thead>
                    <tr style="border: 1px solid black;">
                        <th style="border: 1px solid black; padding: 5px;">Sản phẩm</th>
                        <th style="border: 1px solid black; padding: 5px;">Số lượng</th>
                        <th style="border: 1px solid black; padding: 5px;">Giá tiền</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($cart as $productId => $item)
                        @php
                            $total = 0;
                            $total += $item['sell'] * $item['qty'];
                        @endphp
                        <tr style="border: 1px solid black;">
                            <td style="border: 1px solid black; padding: 5px;">{{ $item['name'] }}</td>
                            <td style="border: 1px solid black; padding: 5px;">{{ $item['qty'] }}</td>
                            <td style="border: 1px solid black; padding: 5px;">{{ number_format($item['sell']) }}VNĐ
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr style="border: 1px solid black;">
                        <td colspan="2" style="border: 1px solid black; padding: 5px;">Tổng cộng</td>
                        <td style="border: 1px solid black; padding: 5px;">{{ number_format($total) }}VNĐ</td>
                    </tr>
                </tfoot>
            </table>
            <div class="single-footer-widget">
                <h6>Về chúng tôi</h6>
                <p>
                    Địa Chỉ: Đường Hoàng Sa, T.T An Châu, Huyện Châu Thành, An Giang.
                </p>
                <p>
                    SĐT:0969610092.
                </p>

            </div>




        </div>

    </section>
    <!--================End Order Details Area =================-->

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('client/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('client/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('client/js/nouislider.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{ asset('client/js/gmaps.min.js') }}"></script>
    <script src="{{ asset('client/js/main.js') }}"></script>
</body>

</html>
