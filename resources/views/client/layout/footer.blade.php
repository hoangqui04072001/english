<footer class="footer-area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Về chúng tôi</h6>
                    <p>
                        Địa Chỉ: Đường Hoàng Sa, T.T An Châu, Huyện Châu Thành, An Giang.
                    </p>
                    <p>
                        SĐT:0969610092.
                    </p>
                    <p>
                        Email: kramaenglish@gmail.com.
                    </p>

                </div>
            </div>
            <div class="col-lg-4  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Các Khóa Học Mới</h6>
                    <p>Luôn luôn được cập nhật mỗi tháng !!!</p>
                    <div class="" id="mc_embed_signup">


                    </div>
                </div>
            </div>
            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget mail-chimp">
                    <h6 class="mb-20">Instragram</h6>
                    <ul class="instafeed d-flex flex-wrap">
                        <li><img src="https://static.anhnguathena.vn/anhngu//img.media/top30tttahanoi/2-british-councel.png"
                                alt="british-council" width="50" height="50"></li>
                        <li><img src="https://static.anhnguathena.vn/anhngu//img.media/top30tttahanoi/3-acet.png"
                                alt="acet" width="50" height="50"></li>
                        <li><img src="https://static.anhnguathena.vn/anhngu//img.media/top30tttahanoi/4-ecorp-english.png"
                                alt="ecorp-english" width="50" height="50"></li>
                        <li><img src="https://static.anhnguathena.vn/anhngu//img.media/top30tttahanoi/6-equest.png"
                                alt="equest" width="50" height="50"></li>
                        <li><img src="https://static.anhnguathena.vn/anhngu//img.media/top30tttahanoi/7-ielts-fighters.png"
                                alt="ielts-fighters" width="50" height="50"></li>
                        <li><img src="https://static.anhnguathena.vn/anhngu//img.media/top30tttahanoi/8-language-link.png"
                                alt="language-link" width="50" height="50"></li>
                        <li><img src="https://static.anhnguathena.vn/anhngu//img.media/top30tttahanoi/9-apax.png"
                                alt="apax-english" width="50" height="50"></li>
                        <li><img src="https://static.anhnguathena.vn/anhngu//img.media/top30tttahanoi/11-ocean-edu.png"
                                alt="ocean-edu" width="50" height="50"></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Theo Dõi</h6>
                    <p>Mạng xã hội chúng tôi </p>
                    <div class="footer-social d-flex align-items-center">
                        <a href="https://www.facebook.com/profile.php?id=100008963505174"><i
                                class="fa fa-facebook"></i></a>
                        {{-- <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-behance"></i></a> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
            <p class="footer-text m-0">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

                Bản quyền ©
                <script>
                    document.write(new Date().getFullYear());
                </script> Tất cả <i class="fa fa-heart-o" aria-hidden="true"></i> thực
                hiện <a href="https://colorlib.com" target="_blank">Hoàng Quý</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
        </div>
    </div>
</footer>
