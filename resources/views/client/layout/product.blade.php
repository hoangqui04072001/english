<section class="section-title">


    <!-- single product slide -->
    <div class="single-product-slider">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <div class="section-title">
                        <h1> Khóa Học giảm giá <br><img src="https://cdn-icons-png.flaticon.com/512/478/478008.png"
                                alt="Sale" title="Sale" width="70" height="70"></h1>

                    </div>
                </div>
            </div>
            <div class="row">
                <!-- single product -->
                @if (isset($products) && $products->count() > 0)
                    @foreach ($products as $item)
                        @if (isset($item->discounts) && $item->discounts !== null)
                            <div class="col-lg-3 col-md-6">
                                <div class="single-product">
                                    <img class="client/img-fluid" src="{{ asset($item->picture) }}" alt="">
                                    <div class="product-details">
                                        <h6>{{ $item->name }}</h6>
                                        <div class="price">
                                            <h6>{{ number_format($item->sell) }} VNĐ</h6>
                                            <h6 class="l-through">{{ number_format($item->discounts->discount) }} VNĐ
                                            </h6>
                                        </div>
                                        <div class="prd-bottom">
                                            <a href="{{ route('client.product.cart', ['id' => $item->id]) }}"
                                                class="social-info add_to_cart">
                                                <span class="ti-bag"></span>
                                                <p class="hover-text">Thêm vào giỏ hàng</p>
                                            </a>
                                            <a href="{{ route('client.product.details', ['id' => $item->id]) }}"
                                                class="social-info">
                                                <span class="lnr lnr-heart"></span>
                                                <p class="hover-text">Chi tiết</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    <nav class="blog-pagination justify-content-center d-flex">
        <ul class="pagination">
            <li class="page-item">
                <a href="{{ $products->previousPageUrl() }}" class="page-link" aria-label="Previous">
                    <span aria-hidden="true">
                        <span class="lnr lnr-chevron-left"></span>
                    </span>
                </a>
            </li>
            @foreach ($products->getUrlRange($products->currentPage() + 0, $products->currentPage() + 2) as $page => $url)
                <li class="page-item {{ $page == $products->currentPage() ? 'active' : '' }}">
                    <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                </li>
            @endforeach
            <li class="page-item">
                <a href="{{ $products->nextPageUrl() }}" class="page-link" aria-label="Next">
                    <span aria-hidden="true">
                        <span class="lnr lnr-chevron-right"></span>
                    </span>
                </a>
            </li>
        </ul>
    </nav>
</section>
