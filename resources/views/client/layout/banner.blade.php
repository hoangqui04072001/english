<section class="banner-area">
    <div class="container">
        <div class="row fullscreen align-items-center justify-content-start">
            <div class="col-lg-12">

                <!-- single-slide -->
                <div class="row single-slide align-items-center d-flex">
                    <div class="col-lg-5 col-md-6">
                        <div class="banner-content">
                            <h1>Khóa Học Tiếng Anh</h1>
                            <p>Nơi sẽ giúp bạn nâng cao
                                trình độ tiếng anh của bản thân lên một tầm cao mới.
                                Và học được nhiều điều thú vị từ tiếng Anh nhé !.</p>
                            <div class="add-bag d-flex align-items-center">
                                <a class="add-btn" href="{{ route('client.home') }}"><span
                                        class="lnr lnr-cross"></span></a>
                                <span class="add-text text-uppercase">Thêm Khóa Học Ngay Thôi !!!</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="banner-img">
                            <img class="img-fluid" src="{{ asset('client/img/banner/banner-img.png') }}" alt="">
                        </div>
                    </div>
                </div>
                <!-- single-slide -->
                <div class="row single-slide">

                </div>

            </div>
        </div>
    </div>
</section>
