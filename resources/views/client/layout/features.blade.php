<section class="features-area section_gap">
    <div class="container">
        <div class="row features-inner">
            <!-- single features -->
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-features">
                    <div class="f-icon">
                        <img src="client/img/features/f-icon1.png" alt="">
                    </div>
                    <h6>Uy Tín Hàng Đầu</h6>
                    <p>được cấp giấy chứng nhận uy tín</p>
                </div>
            </div>
            <!-- single features -->
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-features">
                    <div class="f-icon">
                        <img src="client/img/features/f-icon2.png" alt="">
                    </div>
                    <h6>Chính Sách Hoàn Trả</h6>
                    <p>được hoàn trả chi phí 100%</p>
                </div>
            </div>
            <!-- single features -->
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-features">
                    <div class="f-icon">
                        <img src="client/img/features/f-icon3.png" alt="">
                    </div>
                    <h6>Hỗ Trợ 24/7</h6>
                    <p>sẵn sàng hỏi đáp bất cứ khung giờ</p>
                </div>
            </div>
            <!-- single features -->
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-features">
                    <div class="f-icon">
                        <img src="client/img/features/f-icon4.png" alt="">
                    </div>
                    <h6>Thanh Toán An Toàn</h6>
                    <p>đảm bảo thanh toán an toàn</p>
                </div>
            </div>
        </div>
    </div>
</section>
