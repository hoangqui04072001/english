<section class="category-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12">
                <div class="row">

                    <div class="col-lg-8 col-md-8">
                        <div class="single-deal">
                            <div class="overlay"></div>
                            <img class="client-fluid w-100"
                                src="https://giasuhanoigioi.edu.vn/wp-content/uploads/maxresdefault.jpg" alt="">
                            <a href="client/img/category/c1.jpg" class="img/img-pop-up" target="_blank">
                                <div class="deal-details">
                                    <h6 class="deal-title">{{ @$categories[4]->name }}</h6>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="single-deal">
                            <div class="overlay"></div>
                            <img class="img-fluid w-100"
                                src="https://www.engo.edu.vn/wp-content/uploads/2021/08/IELTS-Prep-683x683.png"
                                height="60px" width="50px" alt="">
                            <a href="client/img/category/c2.jpg" class="img-pop-up" target="_blank">
                                <div class="deal-details">
                                    <h6 class="deal-title">{{ @$categories[3]->name }}</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="single-deal">
                            <div class="overlay"></div>
                            <img class="client/img-fluid w-100"
                                src="https://www.engo.edu.vn/wp-content/uploads/2021/12/vvvvv-683x683.png"
                                alt="">
                            <a href="client/img/category/c3.jpg" class="img-pop-up" target="_blank">
                                <div class="deal-details">
                                    <h6 class="deal-title">{{ @$categories[2]->name }}</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8">
                        <div class="single-deal">
                            <div class="overlay"></div>
                            <img class="client/img-fluid w-100"
                                src="https://theedge.vn/wp-content/uploads/2021/04/khoa-hoc-tieng-anh-giao-tiep-theedge-1-1024x576.png"
                                alt="">
                            <a href="client/img/category/c4.jpg" class="img-pop-up" target="_blank">
                                <div class="deal-details">
                                    <h6 class="deal-title">{{ @$categories[1]->name }}</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="single-deal">
                    <div class="overlay"></div>
                    <img class="client/img-fluid w-100"
                        src="https://stvenglish.edu.vn/wp-content/uploads/2021/08/1-2.png" alt="">
                    <a href="client/img/category/c5.jpg" class="img-pop-up" target="_blank">
                        <div class="deal-details">
                            <h6 class="deal-title">{{ @$categories[0]->name }}</h6>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
