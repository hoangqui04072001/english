<header class="header_area sticky-header">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light main_box">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="{{ route('home') }}"><img src="{{ asset('client/img/logo.png') }}"
                        alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="nav-item {{ request()->routeIs('home') ? 'active' : '' }}"><a class="nav-link"
                                href="{{ route('home') }}">Trang Chủ</a></li>
                        <li class="nav-item submenu dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-haspopup="true" aria-expanded="false">Cửa Hàng</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="nav-link" href="{{ route('client.home') }}">Loại Khóa
                                        Học</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('client.home') }}">Khóa Học</a>
                                </li>



                            </ul>
                        </li>
                        <li class="nav-item submenu dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-haspopup="true" aria-expanded="false">Blog</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="nav-link" href="{{ route('client.blog') }}">Blog</a></li>

                        </li>
                    </ul>
                    </li>
                    <li class="nav-item submenu dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true"
                            aria-expanded="false">{{ Auth::check() ? 'Xin chào, ' . Auth::user()->name : 'Tài Khoản' }}</a>
                        <ul class="dropdown-menu">
                            @if (!Auth::check())
                                <li class="nav-item"><a class="nav-link" href="{{ route('client.login') }}">Đăng
                                        Nhập</a></li>
                            @else
                                <li class="nav-item"><a class="nav-link" href="{{ route('client.orders') }}">Quản lý
                                        đơn hàng</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('check.out') }}">Thanh toán
                                        Khóa
                                        Học</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('client.logout') }}">Đăng
                                        Xuất</a></li>
                            @endif
                        </ul>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('client.contact') }}">Liên Hệ</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item submenu dropdown">
                            <a href="{{ route('client.home') }}" class="cart">
                                <span class="ti-bag"><span class="genric-btn danger circle"
                                        style="font-size: 80%; line-height: 10px; padding: 0 3px;">{{ count((array) session('cart')) }}</span>
                                </span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-left">
                                <li class="nav-item submenu dropdown">
                                    <div class="nav-item">
                                        @php $total = 0 @endphp
                                        @foreach ((array) session('cart') as $id => $details)
                                            @php $total += $details['sell'] * $details['qty'] @endphp
                                        @endforeach
                                        <div class="row">
                                            <p>Tổng Tiền: <span class="text-info">{{ number_format($total) }}
                                                    VNĐ</span>
                                            </p>
                                        </div>

                                        @if (session('cart'))
                                            @foreach (session('cart') as $id => $details)
                                                <div class="row">
                                                    <img src="{{ $details['picture'] }}" alt=""
                                                        style="max-width: 100px; max-height: 100px;">
                                                </div>

                                                <div class="row">
                                                    <p>{{ $details['name'] }}
                                                        <span class="price text-info">
                                                            Giá:{{ number_format($details['sell']) }} VNĐ </span>
                                                    </p>
                                                </div>
                                                <div class="row">
                                                    <p class="count"> Số Lượng:
                                                        {{ $details['qty'] }}</p>

                                                </div>
                                                <p>-------------------------</p>
                                            @endforeach

                                        @endif
                                    </div>
                                    <div class="row">
                                        <a href="{{ route('client.cart') }}" class="genric-btn primary circle">Xem tất
                                            cả</a>
                                    </div>

                                </li>

                            </ul>

                        </li>
                        <li class="nav-item">
                            <button class="search"><span class="lnr lnr-magnifier" id="search"></span></button>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="search_input" id="search_input_box">
        <div class="container">
            <form class="d-flex justify-content-between" id="search_form"
                action="{{ route('client.product.search') }}" method="get">
                <input type="text" name="q" class="form-control" id="search_input" placeholder="Tìm Kiếm">
                <button type="button" class="btn"></button>
                <span class="lnr lnr-cross" id="close_search" title="Close Search"></span>
            </form>
            <ul style="background-color: #fff; max-height: 200px; overflow-y: auto;" id="search_results">

            </ul>
        </div>
    </div>


</header>
@section('script')
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        $('#search_input').on('change', function(e) {
            let searchTerm = $(this).val();
            $.ajax({
                url: "{{ route('client.product.search') }}",
                type: "GET",
                data: {
                    'q': searchTerm
                },
                success: function(response) {
                    if (response.data) {
                        $('#search_results').append(response.data);

                    }

                }

            });
        });
    });
</script>
