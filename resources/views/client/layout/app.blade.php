<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('client/img/fav.png') }}">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Trang Chủ Khóa Học</title>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        .img-sale {
            width: 400px;
            /* Độ rộng của hình ảnh */
            height: 400px;
            /* Độ cao của hình ảnh */
            transition: transform 0.5s ease-in-out;
            /* Thời gian và kiểu chuyển đổi */
        }

        /* Định nghĩa hoạt động của hiệu ứng khi hover vào */
        .img-sale:hover {
            transform: scale(1.1) rotate(5deg);
            /* Phóng to 110% kích thước ban đầu và xoay 5 độ */
        }
    </style>
    <style>
        .my-h4 {
            color: #fff;
            /* Màu chữ */
            background-color: #ff9800;
            /* Màu nền */
            padding: 10px;
            /* Khoảng cách giữa nội dung và viền của thẻ */
            border-radius: 5px;
            /* Bo tròn viền của thẻ */
            text-transform: uppercase;
            /* Chuyển đổi chữ thành in hoa */
            font-weight: bold;
            /* Độ đậm của chữ */
            text-align: center;
            /* Căn giữa nội dung trong thẻ */
            margin-bottom: 20px;
            /* Khoảng cách dưới cùng của thẻ */
            /* Tạo độ đổ bóng */
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
            /* Tạo độ đổ bóng khi hover (nếu cần) */
            transition: box-shadow 0.3s ease-in-out;
        }

        /* Hiệu ứng hover */
        .my-h4:hover {
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3);
        }
    </style>
    <style>
        .img-small {
            width: 30px;
            height: 30px;
            /* Áp dụng chuyển động lắc qua lắc */
            animation: wobble 1s infinite;
            transform-origin: center;
        }

        /* Định nghĩa động lực chuyển động lắc qua lắc */
        @keyframes wobble {
            0% {
                transform: rotate(0deg);
            }

            15% {
                transform: rotate(-15deg);
            }

            30% {
                transform: rotate(15deg);
            }

            45% {
                transform: rotate(-15deg);
            }

            60% {
                transform: rotate(15deg);
            }

            75% {
                transform: rotate(-15deg);
            }

            90% {
                transform: rotate(15deg);
            }

            100% {
                transform: rotate(0deg);
            }
        }
    </style>
    <!--
  CSS
  ============================================= -->
    <link rel="stylesheet" href="{{ asset('client/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/ion.rangeSlider.css') }}" />
    <link rel="stylesheet" href="{{ asset('client/css/ion.rangeSlider.skinFlat.css') }}" />
    <link rel="stylesheet" href="{{ asset('client/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/main.css') }}">
</head>

<body>

    <!-- Start Header Area -->
    @include('client.layout.header')
    <!-- End Header Area -->

    <!-- start banner Area -->
    @include('client.layout.banner')

    <!-- End banner Area -->

    <!-- start features Area -->
    @include('client.layout.features')
    <!-- end features Area -->

    <!-- Start category Area -->
    @include('client.layout.category')
    <!-- End category Area -->

    <!-- start product Area -->
    @include('client.layout.product')
    <!-- end product Area -->

    <!-- Start exclusive deal Area -->
    <section class="exclusive-deal-area">
        <div class="container-fluid">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6 no-padding exclusive-left">
                    <div class="row clock_sec clockdiv" id="clockdiv">
                        <div class="col-lg-12">
                            <h1>Nhanh chân mua khóa học giá siêu ưu đãi!</h1>
                            <p>Người đầu tiên đăng kí mua khóa học sẽ được giảm lên đến 50%.</p>
                            {{-- <img class="img-sale" src="{{ asset('client\img\sale50.png') }}" alt="Tấm hình bùng nổ"
                                width="400" height="400" style=""> --}}
                        </div>
                        <div class="col-lg-12">
                            <div class="row clock-wrap">
                                <div class="col clockinner1 clockinner">
                                    <h1 class="days">29</h1>
                                    <span class="smalltext">Ngày</span>
                                </div>
                                <div class="col clockinner clockinner1">
                                    <h1 class="hours">23</h1>
                                    <span class="smalltext">Giờ</span>
                                </div>
                                <div class="col clockinner clockinner1">
                                    <h1 class="minutes">57</h1>
                                    <span class="smalltext">phút</span>
                                </div>
                                <div class="col clockinner clockinner1">
                                    <h1 class="seconds">19</h1>
                                    <span class="smalltext">Giây</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ asset('client.product') }}" class="primary-btn">Đến Cửa Hàng</a>
                </div>
                <div class="col-lg-6 no-padding exclusive-right">
                    <div class="col-lg-15">
                        <div class="single-exclusive-slider">
                            <iframe width="700" height="300" src="https://www.youtube.com/embed/r3VT34hZvDs"
                                title="YouTube video player" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                allowfullscreen></iframe>

                            <div class="product-details">
                                <div class="price">
                                    <h4>Tại sao bạn nên lựa chọn chúng tôi ??? </h4>

                                    <h6><img src="   https://cdn-icons-png.flaticon.com/512/5290/5290058.png "
                                            width="30" height="30" alt="" title=""
                                            class="img-small"> Là giảng viên và sinh viên xuất sắc tại các trường có các
                                        khoa chuyên tiếng Anh
                                        tại Việt Nam.
                                    </h6>

                                    <h6><img src="   https://cdn-icons-png.flaticon.com/512/5290/5290058.png "
                                            width="30" height="30" alt="" title=""
                                            class="img-small"> Là thạc sĩ hoặc du học viên giỏi tại các nước nói tiếng
                                        Anh.</h6>

                                    <h6><img src="   https://cdn-icons-png.flaticon.com/512/5290/5290058.png "
                                            width="30" height="30" alt="" title=""
                                            class="img-small"> Có các chứng chỉ quốc tế tiếng Anh với điểm số cao
                                        (IELTS, TOEIC, VSTEP…).</h6>

                                    <h6><img src="   https://cdn-icons-png.flaticon.com/512/5290/5290058.png "
                                            width="30" height="30" alt="" title=""
                                            class="img-small"> Có phương pháp dạy khoa học, hiệu quả với từng đối tượng
                                        học viên.</h6>
                                    <br>
                                    <h6><img src="   https://cdn-icons-png.flaticon.com/512/5290/5290058.png "
                                            width="30" height="30" alt="" title=""
                                            class="img-small"> Nhiệt tình và có trách nhiệm trong công việc.</h6>
                                    <br>
                                    <h4 class="my-h4">KARMA – CHƯƠNG TRÌNH HỌC TIẾNG ANH TRỰC TUYẾN VƯỢT TRỘI TỪ ANH
                                        QUỐC.</h4>



                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End exclusive deal Area -->

    <!-- Start brand Area -->
    <section class="brand-area section_gap">
        <div class="container">
            <div class="row">
                <a class="col single-img" href="#">
                    <img class="img-fluid d-block mx-auto"
                        src="https://www.engo.edu.vn/wp-content/uploads/2021/09/Trung-Tam-Ngoai-Ngu-12-1024x1024.png"
                        alt="">
                </a>
                <a class="col single-img" href="#">
                    <img class="img-fluid d-block mx-auto"
                        src="https://www.engo.edu.vn/wp-content/uploads/2021/11/LOGO-SAP-XEP-LAI-07-1024x1024.png"
                        alt="">
                </a>
                <a class="col single-img" href="#">
                    <img class="img-fluid d-block mx-auto"
                        src="https://www.engo.edu.vn/wp-content/uploads/2021/11/LOGO-SAP-XEP-LAI-10-1024x1024.png"
                        alt="">
                </a>
                <a class="col single-img" href="#">
                    <img class="img-fluid d-block mx-auto"
                        src="https://www.engo.edu.vn/wp-content/uploads/2021/09/Dai-Hoc-04-1024x1024.png"
                        alt="">
                </a>
                <a class="col single-img" href="#">
                    <img class="img-fluid d-block mx-auto"
                        src="https://www.engo.edu.vn/wp-content/uploads/2021/09/Dai-Hoc-11-1024x1024.png"
                        alt="">
                </a>
            </div>
        </div>
    </section>
    <!-- End brand Area -->

    <!-- Start related-product Area -->
    {{-- <section class="related-product-area section_gap_bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <div class="section-title">
                        <h1>Deals of the Week</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore
                            magna aliqua.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
                            <div class="single-related-product d-flex">
                                <a href="#"><img src="client/img/r1.jpg" alt=""></a>
                                <div class="desc">
                                    <a href="#" class="title">Black lace Heels</a>
                                    <div class="price">
                                        <h6>$189.00</h6>
                                        <h6 class="l-through">$210.00</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
                            <div class="single-related-product d-flex">
                                <a href="#"><img src="client/img/r2.jpg" alt=""></a>
                                <div class="desc">
                                    <a href="#" class="title">Black lace Heels</a>
                                    <div class="price">
                                        <h6>$189.00</h6>
                                        <h6 class="l-through">$210.00</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
                            <div class="single-related-product d-flex">
                                <a href="#"><img src="client/img/r3.jpg" alt=""></a>
                                <div class="desc">
                                    <a href="#" class="title">Black lace Heels</a>
                                    <div class="price">
                                        <h6>$189.00</h6>
                                        <h6 class="l-through">$210.00</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
                            <div class="single-related-product d-flex">
                                <a href="#"><img src="client/img/r5.jpg" alt=""></a>
                                <div class="desc">
                                    <a href="#" class="title">Black lace Heels</a>
                                    <div class="price">
                                        <h6>$189.00</h6>
                                        <h6 class="l-through">$210.00</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
                            <div class="single-related-product d-flex">
                                <a href="#"><img src="client/img/r6.jpg" alt=""></a>
                                <div class="desc">
                                    <a href="#" class="title">Black lace Heels</a>
                                    <div class="price">
                                        <h6>$189.00</h6>
                                        <h6 class="l-through">$210.00</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
                            <div class="single-related-product d-flex">
                                <a href="#"><img src="client/img/r7.jpg" alt=""></a>
                                <div class="desc">
                                    <a href="#" class="title">Black lace Heels</a>
                                    <div class="price">
                                        <h6>$189.00</h6>
                                        <h6 class="l-through">$210.00</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="single-related-product d-flex">
                                <a href="#"><img src="client/img/r9.jpg" alt=""></a>
                                <div class="desc">
                                    <a href="#" class="title">Black lace Heels</a>
                                    <div class="price">
                                        <h6>$189.00</h6>
                                        <h6 class="l-through">$210.00</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="single-related-product d-flex">
                                <a href="#"><img src="client/img/r10.jpg" alt=""></a>
                                <div class="desc">
                                    <a href="#" class="title">Black lace Heels</a>
                                    <div class="price">
                                        <h6>$189.00</h6>
                                        <h6 class="l-through">$210.00</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="single-related-product d-flex">
                                <a href="#"><img src="client/img/r11.jpg" alt=""></a>
                                <div class="desc">
                                    <a href="#" class="title">Black lace Heels</a>
                                    <div class="price">
                                        <h6>$189.00</h6>
                                        <h6 class="l-through">$210.00</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ctg-right">
                        <a href="#" target="_blank">
                            <img class="img-fluid d-block mx-auto" src="client/img/category/c5.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
    <!-- End related-product Area -->

    <!-- start footer Area -->
    @include('client.layout.footer')
    <!-- End footer Area -->
    <!-- Start of LiveChat (www.livechat.com) code -->
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/643ca64131ebfa0fe7f8aa31/1gu6eisib';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->


    {{-- <script>
        window.__lc = window.__lc || {};
        window.__lc.license = 15334881;;
        (function(n, t, c) {
            function i(n) {
                return e._h ? e._h.apply(null, n) : e._q.push(n)
            }
            var e = {
                _q: [],
                _h: null,
                _v: "2.0",
                on: function() {
                    i(["on", c.call(arguments)])
                },
                once: function() {
                    i(["once", c.call(arguments)])
                },
                off: function() {
                    i(["off", c.call(arguments)])
                },
                get: function() {
                    if (!e._h) throw new Error("[LiveChatWidget] You can't use getters before load.");
                    return i(["get", c.call(arguments)])
                },
                call: function() {
                    i(["call", c.call(arguments)])
                },
                init: function() {
                    var n = t.createElement("script");
                    n.async = !0, n.type = "text/javascript", n.src = "https://cdn.livechatinc.com/tracking.js",
                        t.head.appendChild(n)
                }
            };
            !n.__lc.asyncInit && e.init(), n.LiveChatWidget = n.LiveChatWidget || e
        }(window, document, [].slice))
    </script> --}}
    <noscript><a href="https://www.livechat.com/chat-with/15334881/" rel="nofollow">Chat with us</a>, powered by <a
            href="https://www.livechat.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
    <!-- End of LiveChat code -->
    @yield('script')
    <script src="{{ asset('client/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('client/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('client/js/nouislider.min.js') }}"></script>
    <script src="{{ asset('client/js/countdown.js') }}"></script>
    <script src="{{ asset('client/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{ asset('client/js/gmaps.min.js') }}"></script>
    <script src="{{ asset('client/js/main.js') }}"></script>
</body>

</html>
