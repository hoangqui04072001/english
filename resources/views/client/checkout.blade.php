<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('client/img/fav.png') }}">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Thanh Toán Khóa Học</title>

    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="{{ asset('client/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/main.css') }}">
</head>

<body>

    <!-- Start Header Area -->
    @include('client.layout.header')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Checkout</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Trang Chủ<span class="lnr lnr-arrow-right"></span></a>
                        <a href="single-product.html">Thanh Toán</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->
    <section class="checkout_area section_gap">
        <div class="container">
            <form class="contact_form" action="{{ route('pay') }}" method="post">
                @csrf
                <div class="billing_details">
                    <div class="row">
                        <div class="col-lg-8">
                            <h3>Thông Tin Thanh Toán</h3>
                            <div class="row">
                                <div class="col-md-6 form-group p_star">
                                    <input type="text" class="form-control" id="first" name="order_name"
                                        placeholder="Họ tên" value="{{ Auth::check() ? Auth::user()->name : '' }}"
                                        required>
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <select class="country_select form-control" name="order_gender" required>
                                        <option value="">Vui lòng chọn giới tính</option>
                                        <option value="1">Nam</option>
                                        <option value="2">Nữ</option>
                                        <option value="3">Khác</option>
                                    </select>
                                </div>

                                <div class="col-md-6 form-group p_star">
                                    <input type="text" class="form-control" id="number" name="order_number"
                                        placeholder="Số điện thoại" required>
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="text" class="form-control" id="email" name="order_email"
                                        placeholder="Địa chỉ email"
                                        value="{{ Auth::check() ? Auth::user()->email : '' }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="order_box">
                                <h2>Khóa Học Bạn Muốn Mua</h2>
                                <ul class="list">
                                    @php $total = 0 @endphp
                                    @if (session('cart'))
                                        <li><a href="#">Khóa Học <span>Giá Tiền</span></a></li>
                                        @foreach (session('cart') as $id => $details)
                                            @php $total += $details['sell'] * $details['qty'] @endphp
                                            <li><a href="#">{{ $details['name'] }}
                                                    x{{ $details['qty'] }} <span
                                                        class="last">{{ number_format($details['sell']) }}
                                                        VNĐ</span></a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                                <br>

                                <ul class="list list_2">
                                    <li><a href="#">Tổng Tiền: <span>{{ number_format($total) }}
                                                VNĐ</span></a></li>
                                </ul>

                                <button type="submit" class="btn primary-btn">THANH TOÁN</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!--================End Checkout Area =================-->

    <!-- start footer Area -->
    @include('client.layout.footer')
    <!-- End footer Area -->


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('client/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('client/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('client/js/nouislider.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{ asset('client/js/gmaps.min.js') }}"></script>
    <script src="{{ asset('client/js/main.js') }}"></script>
</body>

</html>
