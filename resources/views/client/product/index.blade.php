<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('client/img/fav.png') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/themes/base/jquery-ui.min.css"
        integrity="sha512-ELV+xyi8IhEApPS/pSj66+Jiw+sOT1Mqkzlh8ExXihe4zfqbWkxPRi8wptXIO9g73FSlhmquFlUOuMSoXz5IRw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Danh Mục Khóa Học</title>


    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="{{ asset('client/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('client/scss/theme/_header.scss') }}">
</head>

<body id="category">

    <!-- Start Header Area -->
    @include('client.layout.header')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>CÁC DANH MỤC VÀ KHÓA HỌC</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Trang Chủ<span class="lnr lnr-arrow-right"></span></a>
                        <a href="#">Cửa Hàng<span class="lnr lnr-arrow-right"></span></a>
                        <a href="category.html">Danh Mục Khóa Học & Khóa Học</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="to container">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-md-5">
                <div class="sidebar-categories">
                    <div class="head">Danh Mục Khóa Học</div>
                    <ul class="main-categories">
                        @foreach ($category as $item)
                            <li class="main-nav-list">
                                <a href="{{ route('client.product', ['category_id' => $item->id]) }}">
                                    {{ $item->name }}
                                    <span class="lnr lnr-arrow-right">
                                    </span>
                                </a>
                                {{-- <span class="number">(53)</span> --}}
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="sidebar-filter mt-50">
                    <div class="top-filter-head">Lựa Chọn Giá</div>

                    <form action="{{ route('client.home') }}" method="get">
                        @csrf
                        <label for="amount">Lọc giá theo</label>
                        <div id="slider-range"></div>
                        <input type="text" id="amount" readonly
                            style="border:2; color:#f6931f;  width: 100%; font-weight:bold; border-radius:10px">
                        <input type="hidden" name="start_price" id="start_price">
                        <input type="hidden" name="end_price" id="end_price">
                        <br>
                        <input type="submit" style=" color:#f6931f; font-weight:bold; border-radius:10px"
                            name="filter_price" value="lọc giá" class="btn btn-default">
                    </form>


                </div>
            </div>
            <div class="col-xl-9 col-lg-8 col-md-7">
                <!-- Start Filter Bar -->
                <div class="filter-bar d-flex flex-wrap align-items-center">
                    <div class="sorting">
                        <form action="{{ route('client.home') }}" method="post">
                            @csrf
                            @if (isset($tenloai))
                                <input type="hidden" id="tenloai_slug" name="tenloai_slug"
                                    value="{{ Str::slug($tenloai, '-') }}" />
                            @endif
                            <select class="form-control form-control-sm" id="sapxep" name="sapxep"
                                onchange="if(this.value != 0) { this.form.submit(); }">
                                <option value="default" {{ session('sapxep') == 'default' ? 'selected' : '' }}>Sắp xếp
                                    mặc định</option>

                                <option value="date" {{ session('sapxep') == 'date' ? 'selected' : '' }}>Hàng mới
                                    nhất</option>
                                <option value="price" {{ session('sapxep') == 'price' ? 'selected' : '' }}>Xếp theo
                                    giá: thấp đến cao</option>
                                <option value="price-desc" {{ session('sapxep') == 'price-desc' ? 'selected' : '' }}>
                                    Xếp theo giá: cao xuống thấp</option>
                            </select>
                        </form>
                    </div>


                </div>
                <!-- End Filter Bar -->
                <!-- Start Best Seller -->
                <section class="lattest-product-area pb-40 category-list">
                    <div class="row">

                        <!-- single product -->
                        @foreach ($products as $product)
                            <div class="col-lg-3 col-md-6">

                                <div class="single-product">
                                    <img class="img-fluid" src="{{ asset($product->picture) }}" alt="">
                                    <div class="product-details">
                                        <h6>{{ $product->name }}</h6>
                                        <div class="price">
                                            <h6>{{ number_format($product->sell) }} VNĐ</h6>
                                            @if ($product->discounts)
                                                @if ($product->discounts->count() > 0)
                                                    <h6 class="l-through">
                                                        {{ number_format($product->discounts->discount) }} VNĐ</h6>
                                                @endif
                                            @else
                                                <h5 style="opacity: 0;">
                                                    (không giảm giá)</h5>
                                            @endif

                                        </div>
                                        <div class="product_count">
                                            <label for="qty">Số lượng:</label>
                                            <input type="number" name="qty" id="sst" maxlength="12"
                                                value="1" title="Quantity:" class="input-text qty">

                                        </div>
                                        <div class="prd-bottom">

                                            <a href="{{ route('client.product.cart', ['id' => $product->id]) }}"
                                                class="social-info add_to_cart">
                                                <span class="ti-bag"></span>
                                                <p class="hover-text">Thêm vào giỏ hàng</p>
                                            </a>
                                            <a href="{{ route('client.product.details', ['id' => $product->id]) }}"
                                                class="social-info">
                                                <span class="lnr lnr-heart"></span>
                                                <p class="hover-text">Chi tiết</p>
                                            </a>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endforeach


                    </div>
                </section>
                <!-- End Best Seller -->
                <!-- Start Filter Bar -->
                <nav class="blog-pagination justify-content-center d-flex">
                    <ul class="pagination">
                        <li class="page-item">
                            <a href="{{ $products->previousPageUrl() }}" class="page-link" aria-label="Previous">
                                <span aria-hidden="true">
                                    <span class="lnr lnr-chevron-left"></span>
                                </span>
                            </a>
                        </li>
                        @foreach ($products->getUrlRange($products->currentPage() + 0, $products->currentPage() + 2) as $page => $url)
                            <li class="page-item {{ $page == $products->currentPage() ? 'active' : '' }}">
                                <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                            </li>
                        @endforeach
                        <li class="page-item">
                            <a href="{{ $products->nextPageUrl() }}" class="page-link" aria-label="Next">
                                <span aria-hidden="true">
                                    <span class="lnr lnr-chevron-right"></span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- End Filter Bar -->
            </div>
        </div>
    </div>
    <!-- End Banner Area -->

    <!--================Single Product Area =================-->

    @include('client.layout.footer')
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/643ca64131ebfa0fe7f8aa31/1gu6eisib';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('client/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('client/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('client/js/nouislider.min.js') }}"></script>
    <script src="{{ asset('client/js/countdown.js') }}"></script>
    <script src="{{ asset('client/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{ asset('client/js/gmaps.min.js') }}"></script>
    <script src="{{ asset('client/js/main.js') }}"></script>
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/jquery-ui.min.js"></script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var productsContainer = document.getElementById('products-container');
            if (!productsContainer) {
                console.error('Phần tử với id "products-container" không tồn tại trên trang.');
                return;
            }

            var lowerValueElement = document.getElementById('lower-value');
            var upperValueElement = document.getElementById('upper-value');

            // Lấy giá trị giới hạn dưới và giới hạn trên từ thanh trượt
            var lowerLimit = parseInt(lowerValueElement.innerText);
            var upperLimit = parseInt(upperValueElement.innerText);

            // Bắt sự kiện khi nút "Lọc giá" được nhấn
            document.getElementsByName('filter_price')[0].addEventListener('click', function() {
                // Gửi yêu cầu AJAX để lấy danh sách sản phẩm dựa trên giá trị giới hạn
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === XMLHttpRequest.DONE) {
                        if (xhr.status === 200) {
                            // Nếu yêu cầu thành công, hiển thị kết quả lên trang
                            var productsContainer = document.getElementById('products-container');
                            productsContainer.innerHTML = xhr.responseText;
                        } else {
                            console.error('Yêu cầu lọc giá không thành công. Mã lỗi: ' + xhr.status);
                        }
                    }
                };
                xhr.open('GET', '/client/product/homesort?lowerLimit=' + lowerLimit + '&upperLimit=' +
                    upperLimit);
                xhr.send();
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $("#slider-range").slider({
                range: true,
                min: 10000,
                max: 10000000,
                values: [50000, 5000000],
                step: 10000,
                slide: function(event, ui) {
                    $("#amount").val(ui.values[0].toLocaleString('vi-VN') + "VNĐ" + " - " + ui
                        .values[1].toLocaleString('vi-VN') +
                        "VNĐ");
                    $("#start_price").val(ui.values[0]);
                    $("#end_price").val(ui.values[1]);
                }
            });

            $("#amount").val($("#slider-range").slider("values", 0).toLocaleString('vi-VN') + "VNĐ" +
                " - " + $("#slider-range").slider("values", 1).toLocaleString('vi-VN') +
                "VNĐ");
        });
    </script>



</body>

</html>
