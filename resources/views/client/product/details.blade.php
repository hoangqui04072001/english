<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('client/img/fav.png') }}">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Danh Mục Khóa Học</title>
    <style>
        /* Định nghĩa mã CSS */
        .fireworks {
            animation-name: fireworks;
            animation-duration: 2s;
            animation-timing-function: ease;
            animation-iteration-count: infinite;
        }

        @keyframes fireworks {
            0% {
                transform: scale(1);
                opacity: 1;
            }

            50% {
                transform: scale(1.5);
                opacity: 0.5;
            }

            100% {
                transform: scale(1);
                opacity: 1;
            }
        }
    </style>
    <style>
        /* Áp dụng cho thẻ h5 có class .l-through */
        h5.l-through {
            text-decoration: line-through;
            /* Tạo gạch ngang cho đoạn văn bản */
        }
    </style>

    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="{{ asset('client/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/main.css') }}">
</head>

<body id="category">

    <!-- Start Header Area -->
    @include('client.layout.header')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Chi Tiết Khóa Học</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Trang Chủ<span class="lnr lnr-arrow-right"></span></a>
                        <a href="#">Khóa Học<span class="lnr lnr-arrow-right"></span></a>
                        <a href="single-product.html">Chi Tiết Khóa Học</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Single Product Area =================-->
    <div class="product_image_area">
        <div class="container">
            <div class="row s_product_inner">
                <div class="col-lg-6">
                    <div class="s_Product_carousel">
                        <div class="single-prd-item">
                            <img class="img-fluid" src="{{ asset($product->picture) }}" alt="">
                        </div>
                        <div class="single-prd-item">
                            <img class="img-fluid" src="{{ asset($product->picture) }}" alt="">
                        </div>
                        <div class="single-prd-item">
                            <img class="img-fluid" src="{{ asset($product->picture) }}" alt="">
                        </div>

                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">
                    <div class="s_product_text">
                        <h3>{{ $product->name }}</h3>
                        <h2>{{ number_format($product->sell) }} VNĐ</h2>
                        <div class="price">
                            @if ($product->discounts)
                                @if ($product->discounts->count() > 0)
                                    <h6 class="l-through">
                                        {{ number_format($product->discounts->discount) }} VNĐ</h6>
                                @endif
                            @else
                                <h5 style="opacity: 0;">
                                    (không giảm giá)</h5>
                            @endif
                        </div>
                        <ul class="list">
                            <li><a class="active" href="#"><span>Thuộc Loại</span>:
                                    {{ $product->Category->name }}</a></li>
                            <li><a href="#"><span>Tình Trạng</span> : Còn Hàng</a></li>
                        </ul>
                        <p>{{ $product->description }}</p>
                        <div class="product_count">
                            <label for="qty">Số Lượng:</label>
                            <input type="number" name="qty" id="sst" maxlength="12" min="1"
                                value="1" title="Quantity:" class="input-text qty">

                        </div>
                        <div class="card_area d-flex align-items-center">
                            <a class="primary-btn"
                                href="{{ route('client.product.cart', ['id' => $product->id]) }}">Thêm vào giỏ hàng</a>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <section class="product_description_area">
        <div class="container">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab"
                        aria-controls="home" aria-selected="true">Mô tả</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                        aria-controls="profile" aria-selected="false">So Sách</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                        aria-controls="contact" aria-selected="false">Bình Luận</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="review-tab" data-toggle="tab" href="#review" role="tab"
                        aria-controls="review" aria-selected="false">Đánh Giá</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <p>Trung tâm tiếng anh: "KARMA"</p>
                    <p> Mô tả tổng quan: Trung tâm tiếng anh "KARMA" là một trang web chuyên cung cấp khóa học tiếng Anh
                        trực
                        tuyến, đem lại trải nghiệm học tập đầy kịch tính và nâng cao khả năng giao tiếp bằng tiếng Anh
                        cho người học.</p>
                    <p>Trung tâm "KARMA" cung cấp nhiều khóa học tiếng Anh khác nhau từ cơ bản đến nâng cao, phù hợp
                        với nhu cầu và trình độ của người học. Các khóa học được thiết kế bài bản, giúp người học nắm
                        vững các kỹ năng nghe, nói, đọc, viết và ngữ pháp tiếng Anh. Nội dung khóa học phong phú, đa
                        dạng và thú vị, bao gồm các bài học, bài kiểm tra, hoạt động thực hành, video, đồ họa hấp dẫn và
                        các tài liệu học tập bổ trợ.

                        Giảng viên chuyên nghiệp: Trung tâm "KARMA" có đội ngũ giảng viên nhiều kinh nghiệm trong
                        giảng dạy tiếng Anh, đảm bảo chất lượng giảng dạy cao nhất. Các giảng viên sử dụng phương pháp
                        giảng dạy hiện đại, tận tình hướng dẫn từng học viên và đồng hành trong quá trình học tập, giúp
                        người học đạt được mục tiêu học tập của mình.

                        Nền tảng học tập tiện lợi: Trung tâm "KARMA" cung cấp môi trường học tập trực tuyến tiện
                        lợi, giúp người học dễ dàng truy cập và tham gia vào các khóa học mọi lúc, mọi nơi, trên mọi
                        thiết bị. Nền tảng học tập cũng tích hợp các công nghệ tiên tiến, đồng bộ hóa dữ liệu học tập,
                        cung cấp đánh giá kết quả học tập, hỗ trợ học tiếng anh.</p>

                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        <h5>Hiệu Quả</h5>
                                    </td>
                                    <td>
                                        <h5>
                                            <img src="   https://cdn-icons-png.flaticon.com/512/9426/9426997.png "
                                                width="25" height="25" alt="" title=""
                                                class="img-small">
                                        </h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h5>Giảng Viên Trình Độ Cao</h5>
                                    </td>
                                    <td>
                                        <h5><img src="   https://cdn-icons-png.flaticon.com/512/9426/9426997.png "
                                                width="25" height="25" alt="" title=""
                                                class="img-small"></h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h5>Linh Hoạt Môi Trường Dạy</h5>
                                    </td>
                                    <td>
                                        <h5><img src="   https://cdn-icons-png.flaticon.com/512/9426/9426997.png "
                                                width="25" height="25" alt="" title=""
                                                class="img-small"></h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h5>Chất Lượng</h5>
                                    </td>
                                    <td>
                                        <h5><img src="   https://cdn-icons-png.flaticon.com/512/9426/9426997.png "
                                                width="25" height="25" alt="" title=""
                                                class="img-small"></h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h5>Học Viên Khắp Cả Nước</h5>
                                    </td>
                                    <td>
                                        <h5><img src="   https://cdn-icons-png.flaticon.com/512/9426/9426997.png "
                                                width="25" height="25" alt="" title=""
                                                class="img-small"></h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h5>Tỷ Lệ Hoàn Thành</h5>
                                    </td>
                                    <td>
                                        <h5 class="fireworks">Trên 92%</h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h5>Chứng Nhận Bộ Giáo Dục & Đào Tạo</h5>
                                    </td>
                                    <td>
                                        <h5><img src="   https://cdn-icons-png.flaticon.com/512/9426/9426997.png "
                                                width="25" height="25" alt="" title=""
                                                class="img-small"></h5>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="comment_list" style="max-height: 400px; overflow-y: auto;">

                                @if (isset($comments) && !empty($comments))
                                    @foreach ($comments as $comment)
                                        <div class="review_item">
                                            <div class="media">
                                                <div class="d-flex">
                                                    <img src="https://cdn-icons-png.flaticon.com/512/1326/1326377.png"
                                                        width="35px" height="35px" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <h4>{{ $comment->name }}</h4>
                                                    <h5>{{ $comment->created_at->format('d-m-Y') }}</h5>
                                                    <a class="reply_btn" href="#">Trả Lời</a>
                                                </div>
                                            </div>
                                            <p>{{ $comment->comment }}</p>
                                        </div>
                                    @endforeach
                                @else
                                    <p>Không có dữ liệu để hiển thị</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="review_box">
                                <div id="comment-message"></div>
                                <h4>Đăng Bình Luận</h4>
                                <form>
                                    <input type="hidden" name="product_id" class="comment_product_id"
                                        value="{{ $product->id }}">
                                    <div id="comment-message"></div>
                                </form>
                                <form class="row contact_form" id="contactForm" novalidate="novalidate">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control comment_product_name"
                                                id="name" name="name" placeholder="Họ Và Tên">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control comment_product_email"
                                                id="email" name="email" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control comment_product_phone"
                                                id="number" name="number" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control comment_product_comment" name="comment" id="message" rows="1"
                                                placeholder="Bình Luận"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="button" class="btn primary-btn send-comment">
                                            Đăng</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade show active" id="review" role="tabpanel" aria-labelledby="review-tab">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row total_rate">
                                <div class="col-6">
                                    <div class="box_total">
                                        <h5>Trung Bình</h5>
                                        <h4>{{ $averageRating }}</h4>
                                        <h6>(các Đánh Giá)</h6>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="rating_list">
                                        <h3>Các số sao đánh giá</h3>
                                        <ul class="list">
                                            <li><a href="#">5 Star <i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="fa fa-star"></i>
                                                    {{ $fiveStarCount }}</a></li>
                                            <li><a href="#">4 Star <i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="a"></i>
                                                    {{ $fourStarCount }}</a></li>
                                            <li><a href="#">3 Star <i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                        class="a"></i><i class="a"></i>
                                                    {{ $threeStarCount }}</a></li>
                                            <li><a href="#">2 Star <i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="a"></i><i
                                                        class="a"></i><i class="a"></i>
                                                    {{ $twoStarCount }}</a></li>
                                            <li><a href="#">1 Star <i class="fa fa-star"></i><i
                                                        class="a"></i><i class="a"></i><i
                                                        class="a"></i><i class="a"></i>
                                                    {{ $oneStarCount }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="review_list">
                                @if (isset($reviews) && !empty($reviews))
                                    @foreach ($reviews as $item)
                                        <div class="review_item">
                                            <div class="media">
                                                <div class="d-flex">
                                                    <img src="   https://cdn-icons-png.flaticon.com/512/4333/4333609.png "
                                                        width="25" height="25" alt="" title=""
                                                        class="img-small">
                                                </div>
                                                <div class="media-body">
                                                    <h4>{{ $item->name }}</h4>
                                                    <div class="rating">
                                                        @for ($i = 1; $i <= 5; $i++)
                                                            @if ($item->rating >= $i)
                                                                <i class="fa fa-star"></i>
                                                            @else
                                                                <i class="fa fa-star-o"></i>
                                                            @endif
                                                        @endfor
                                                    </div>

                                                </div>
                                            </div>
                                            <p>{{ $item->review }}</p>
                                        </div>
                                    @endforeach
                                @else
                                    <p>Không có đánh giá nào để hiển thị</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            @if (session('rating'))
                                <div class="alert alert-success" id="ratingAlert">
                                    Bạn đã đánh giá {{ session('rating') }} sao.
                                </div>
                            @endif
                            <form action="{{ route('reviews.store') }}" method="post">
                                @csrf
                                <div class="review_box">
                                    <h4>Tạo Đánh Giá</h4>
                                    <p>Sao Đánh Giá:</p>
                                    <ul class="list">
                                        <li><a href="#" class="star" data-value="1"><i
                                                    class="fa fa-star"></i></a></li>
                                        <li><a href="#" class="star" data-value="2"><i
                                                    class="fa fa-star"></i></a></li>
                                        <li><a href="#" class="star" data-value="3"><i
                                                    class="fa fa-star"></i></a></li>
                                        <li><a href="#" class="star" data-value="4"><i
                                                    class="fa fa-star"></i></a></li>
                                        <li><a href="#" class="star" data-value="5"><i
                                                    class="fa fa-star"></i></a></li>
                                    </ul>
                                    <p id="ratingText">click vào để chọn</p>

                                    <input type="hidden" id="rating" name="rating" value="1">




                                    <!-- Giá trị rating được đưa vào input này -->
                                    <input type="hidden" id="product_id" name="product_id"
                                        value="{{ $product->id }}"> <!-- Lấy id của sản phẩm để đánh giá -->

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name" name="name"
                                                placeholder="Your Full name" onfocus="this.placeholder = ''"
                                                onblur="this.placeholder = 'Your Full name'">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control" name="review" id="review" rows="1" placeholder="Review"
                                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Review'"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="primary-btn">Gửi Đánh Giá</button>
                                    </div>

                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('client.layout.footer')

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('client/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('client/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('client/js/nouislider.min.js') }}"></script>
    {{-- <script src="{{ asset('client/js/countdown.js') }}"></script> --}}
    <script src="{{ asset('client/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{ asset('client/js/gmaps.min.js') }}"></script>
    <script src="{{ asset('client/js/main.js') }}"></script>
    <script type="text/javascript">
        $('.send-comment').click(function() {
            var comment_product_id = $('.comment_product_id').val();
            var comment_product_name = $('.comment_product_name').val();
            var comment_product_email = $('.comment_product_email').val();
            var comment_product_phone = $('.comment_product_phone').val();
            var comment_product_comment = $('.comment_product_comment').val();
            var _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{ route('client.product.comment') }}",
                method: "POST",
                data: {
                    comment_product_id: comment_product_id,
                    comment_product_name: comment_product_name,
                    comment_product_email: comment_product_email,
                    comment_product_phone: comment_product_phone,
                    comment_product_comment: comment_product_comment,
                    _token: _token
                },
                success: function(data) {
                    window.location.reload();
                    $('#comment-message').text(data.message);
                    $('#comment-message').css('color', 'green');
                    setTimeout(function() {
                        $('#comment-message').text('');
                        $('#comment-message').css('color', '');
                    }, 2000);
                }
            })
        })
    </script>
    <script>
        const starLinks = document.querySelectorAll('.star');
        starLinks.forEach(starLink => {
            starLink.addEventListener('click', function(e) {
                e.preventDefault();
                const value = this.getAttribute(
                    'data-value');
                var rating = this.getAttribute('data-value');
                document.getElementById('rating').value = rating;
                starLinks.forEach(link => {
                    link.style.color = 'gray';
                });

                for (let i = 0; i < value; i++) {
                    starLinks[i].style.color = 'yellow';
                }
            });
        });
    </script>

    <script>
        setTimeout(function() {
            var ratingAlert = document.getElementById('ratingAlert');
            if (ratingAlert) {
                ratingAlert.style.display = 'none';
            }
        }, 2500);
    </script>
    {{-- <script type="text/javascript">
        $(document).ready(function() {
            $('.star').on('click', function() {
                var rating = $(this).data('value');
                // console.log(rating);
                var _token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: 'POST',
                    url: '{{ route('reviews.store') }}',
                    data: {
                        name: name,
                        review: review,
                        rating: rating,
                        _token: _token
                    },
                    success: function(data) {

                        alert(
                            'Đánh giá của bạn đã được gửi thành công!'
                        );
                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                        alert('Đã xảy ra lỗi. Vui lòng thử lại sau.');
                    }
                });
            });
        });
    </script> --}}

</body>

</html>
