<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('client/img/fav.png') }}">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Đơn hàng</title>

    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="{{ asset('client/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/main.css') }}">
</head>

<body>

    <!-- Start Header Area -->
    @include('client.layout.header')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Thông Tin Khóa Học</h1>
                    <nav class="d-flex align-items-center">
                        <a href="{{ route('home') }}">Trang Chủ<span class="lnr lnr-arrow-right"></span></a>
                        <a href="category.html">Thông tin khóa học</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Cart Area =================-->
    <section class="cart_area">
        <div class="container">
            <div class="to container">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
            <div class="cart_inner">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">STT</th>
                                <th scope="col">Mã đơn</th>
                                <th scope="col">Họ tên</th>
                                <th scope="col">Số điện thoại</th>
                                <th scope="col">Email</th>
                                <th scope="col">Giới tính</th>
                                <th scope="col">Thời gian đặt</th>
                                <th scope="col">Trạng thái</th>
                                <th scope="col">Chức Năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $key => $order)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->order_name }}</td>
                                    <td>{{ $order->order_phone }}</td>
                                    <td>{{ $order->order_email }}</td>
                                    <td>{{ $order->order_gender == 1 ? 'Nam' : ($order->order_gender == 2 ? 'Nữ' : 'Khác') }}
                                    </td>
                                    <td>{{ date('d/m/Y H:i:s', strtotime($order->created_at)) }}</td>
                                    <td>{{ $order->status == 1 ? 'Đã thanh toán' : 'Chưa thanh toán' }}</td>
                                    <td>
                                        <a href="{{ route('client.order.detail', ['id' => $order->id]) }}"
                                            class="btn btn-sm btn-primary">Chi tiết</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--================End Cart Area =================-->

    <!-- start footer Area -->
    @include('client.layout.footer')
    <!-- End footer Area -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('client/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('client/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('client/js/nouislider.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{ asset('client/js/gmaps.min.js') }}"></script>
    <script src="{{ asset('client/js/main.js') }}"></script>
    <script>
        // Xử lý sự kiện click nút "-"
        $(document).on('click', '.minus', function() {
            var qtyInput = $(this).siblings('.qty'); // Lấy đối tượng input số lượng
            var qty = parseInt(qtyInput.val()); // Lấy giá trị số lượng hiện tại
            if (qty > 1) {
                qtyInput.val(qty - 1); // Giảm giá trị số lượng đi 1
            }
        });

        // Xử lý sự kiện click nút "+"
        $(document).on('click', '.plus', function() {
            var qtyInput = $(this).siblings('.qty'); // Lấy đối tượng input số lượng
            var qty = parseInt(qtyInput.val()); // Lấy giá trị số lượng hiện tại
            qtyInput.val(qty + 1); // Tăng giá trị số lượng lên 1
        });
    </script>
    {{-- <script>
        function removetocard(event) {
            // event.preventDefault();
            alert(123);

        }
        $(function() {
            $('.cart_remove').on('click', removetocard);
        });
    </script> --}}
    <script type="text/javascript">
        $(".cart_remove").click(function(e) {
            e.preventDefault();

            var ele = $(this);
            if (confirm("Bạn muốn xóa khóa học này ?")) {
                $.ajax({
                    url: '{{ route('remove_from_cart') }}',
                    method: 'DELETE',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: ele.parents("tr").attr("data-id")
                    },
                    success: function(response) {
                        window.location.reload();
                    }
                });
            }
        });
    </script>
    <script type="text/javascript">
        $(".cart_update").change(function(e) {
            e.preventDefault();

            var ele = $(this);

            $.ajax({
                url: '{{ route('update_from_cart') }}',
                method: 'patch',
                data: {
                    _token: '{{ csrf_token() }}',
                    id: ele.parents("tr").attr("data-id"),
                    qty: ele.parents("tr").find(".qty").val()
                },
                success: function(response) {
                    window.location.reload();
                }
            });

        });
    </script>
</body>

</html>
