<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('client/img/fav.png') }}">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Chi tiết khóa học</title>


    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="{{ asset('client/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/main.css') }}">
</head>

<body>

    <!-- Start Header Area -->
    @include('client.layout.header')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Chi Tiết Khóa Học</h1>
                    <nav class="d-flex align-items-center">
                        <a href="{{ route('home') }}">Trang Chủ<span class="lnr lnr-arrow-right"></span></a>
                        <a href="">Chi tiết khóa học</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Cart Area =================-->
    <section class="cart_area">
        <div class="container">
            <div class="to container">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
            <div class="cart_inner">
                <table class="table table-hover" style=" font-size: 1.4em; ">
                    <h3>Danh sách video</h3>
                    <tr>

                        <th>Name</th>
                        <th>Video</th>
                    <tr>
                        @foreach ($videoProducts as $videoProduct)
                    <tr>
                        <td>
                            {{ $videoProduct->name }}
                        </td>
                        <td> <video width="320" height="240" controls>
                                <source src="{{ asset('upload/' . $videoProduct->video) }}" type="video/mp4">
                            </video>
                        </td>
                    </tr>
                    @endforeach
                </table>


                <div>{{ $videoProducts->links() }}</div>
            </div>
        </div>
    </section>
    <!--================End Cart Area =================-->

    <!-- start footer Area -->
    @include('client.layout.footer')
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/643ca64131ebfa0fe7f8aa31/1gu6eisib';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!-- End footer Area -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('client/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('client/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('client/js/nouislider.min.js') }}"></script>
    <script src="{{ asset('client/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{ asset('client/js/gmaps.min.js') }}"></script>
    <script src="{{ asset('client/js/main.js') }}"></script>
    <script>
        // Xử lý sự kiện click nút "-"
        $(document).on('click', '.minus', function() {
            var qtyInput = $(this).siblings('.qty'); // Lấy đối tượng input số lượng
            var qty = parseInt(qtyInput.val()); // Lấy giá trị số lượng hiện tại
            if (qty > 1) {
                qtyInput.val(qty - 1); // Giảm giá trị số lượng đi 1
            }
        });

        // Xử lý sự kiện click nút "+"
        $(document).on('click', '.plus', function() {
            var qtyInput = $(this).siblings('.qty'); // Lấy đối tượng input số lượng
            var qty = parseInt(qtyInput.val()); // Lấy giá trị số lượng hiện tại
            qtyInput.val(qty + 1); // Tăng giá trị số lượng lên 1
        });
    </script>
    {{-- <script>
        function removetocard(event) {
            // event.preventDefault();
            alert(123);

        }
        $(function() {
            $('.cart_remove').on('click', removetocard);
        });
    </script> --}}
    <script type="text/javascript">
        $(".cart_remove").click(function(e) {
            e.preventDefault();

            var ele = $(this);
            if (confirm("Bạn muốn xóa khóa học này ?")) {
                $.ajax({
                    url: '{{ route('remove_from_cart') }}',
                    method: 'DELETE',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: ele.parents("tr").attr("data-id")
                    },
                    success: function(response) {
                        window.location.reload();
                    }
                });
            }
        });
    </script>
    <script type="text/javascript">
        $(".cart_update").change(function(e) {
            e.preventDefault();

            var ele = $(this);

            $.ajax({
                url: '{{ route('update_from_cart') }}',
                method: 'patch',
                data: {
                    _token: '{{ csrf_token() }}',
                    id: ele.parents("tr").attr("data-id"),
                    qty: ele.parents("tr").find(".qty").val()
                },
                success: function(response) {
                    window.location.reload();
                }
            });

        });
    </script>
</body>

</html>
