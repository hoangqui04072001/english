<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClassController;
use App\Http\Controllers\Client\BlogViewController;
use App\Http\Controllers\Client\CartController;
use App\Http\Controllers\Client\CheckOutController;
use App\Http\Controllers\Client\ClientProductController;
use App\Http\Controllers\Client\ContactController;
use App\Http\Controllers\Client\EmailController;
use App\Http\Controllers\Client\HomeController;
use App\Http\Controllers\Client\LoginController;
use App\Http\Controllers\Client\OrderController as ClientOrders;
use App\Http\Controllers\ContactController as ControllersContactController;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\LoginUserController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VideoProductController;
use App\Http\Livewire\HeaderSearchComponent;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/client/cart', [HomeController::class, 'cart'])->name('client.cart');
Route::get('client/contact', [ContactController::class,'index'])->name('client.contact');
Route::post('client/contact', [ContactController::class,'store'])->name('client.contact');
Route::get('client/blog', [BlogViewController::class,'index'])->name('client.blog');
Route::get('client/blog/details/{id}', [BlogViewController::class, 'blog_details'])->name('client.blog.details');

Route::post('client/blog/sreach', [BlogViewController::class,'sreach'])->name('client.blog.sreach');
Route::get('client/product/search', [ClientProductController::class,'search'])->name('client.product.search');


Route::get('client/product', [ClientProductController::class, 'home'])->name('client.home');
Route::post('client/product', [ClientProductController::class, 'homesort'])->name('client.home');
Route::get('client/product/details/{id}', [ClientProductController::class, 'product_details'])->name('client.product.details');
Route::post('client/product/details/reviews', [ClientProductController::class, 'reviews'])->name('reviews.store');
Route::post('client/product/comment', [ClientProductController::class, 'comment'])->name('client.product.comment');
Route::get('client/product/{category_id}', [ClientProductController::class, 'index'])->name('client.product');
Route::get('client/product/cart/{id}', [ClientProductController::class, 'addtocart'])->name('client.product.cart');
Route::delete('client/product/cart/remove-from-cart', [ClientProductController::class, 'remove'])->name('remove_from_cart');
Route::patch('client/product/cart/update-from-cart', [ClientProductController::class, 'update'])->name('update_from_cart');

Route::get('client/register', [LoginController::class, 'register'])->name('client.register');
Route::get('client/login', [LoginController::class, 'login'])->name('client.login');
Route::post('client/register', [LoginController::class, 'store_register'])->name('client.register.store');
Route::post('client/login', [LoginController::class, 'store_login'])->name('client.login.store');

Route::get('client/forget', [LoginController::class, 'forget'])->name('client.forget');
Route::post('client/forget', [LoginController::class, 'store_forget'])->name('client.forget.store');

Route::middleware(['checkLogin'])->group(function () {
    Route::get('client/checkout', [CheckOutController::class, 'index'])->name('check.out');
    Route::post('client/checkout', [CheckOutController::class, 'pay'])->name('pay');
    Route::get('done-payment-vnpay', [CheckOutController::class, 'donePaymentVnpay'])->name('done.payment.vnpay');
    Route::get('cancel-payment-paypal', [CheckOutController::class, 'cancelPaymentPaypal'])->name('cancel.payment.paypal');
    Route::get('done-payment-paypal/{id}', [CheckOutController::class, 'donePaymentPaypal'])->name('done.payment.paypal');
    Route::get('client/logout', [LoginController::class, 'logout'])->name('client.logout');
    Route::get('orders', [ClientOrders::class, 'index'])->name('client.orders');
    Route::get('order/id/{id}', [ClientOrders::class, 'show'])->name('client.order.detail');
    Route::get('product/id/{id}', [ClientOrders::class, 'showDetailProduct'])->name('client.product.detail');
    Route::get('client/check/email', [EmailController::class, 'checkemail'])->name('client.check.mail');
    Route::get('client/email', [EmailController::class, 'email'])->name('client.mail');
});
Auth::routes();
Route::get('/loginUser', function () {
    return view('logins.login');
})->name('loginUser');
Route::get('/registerUser', function () {
    return view('logins.register');
})->name('registerUser');
Route::post('/registerUser', [RegisterController::class, 'store'])->name('registerUser.store');
Route::post('/loginUser', [LoginUserController::class, 'store'])->name('loginUser.store');
Route::get('/logout', [LoginUserController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {
    Route::prefix('administrator')->group(function () {
        Route::middleware('checkRole')->group(function () {
            Route::resource('user', UserController::class);
            Route::resource('role', RoleController::class);
            Route::resource('category', CategoryController::class);
            Route::resource('product', ProductController::class);
            Route::resource('videoproduct', VideoProductController::class);
            Route::resource('teacher', TeacherController::class);
            Route::resource('student', StudentController::class);
            Route::resource('order', OrderController::class);
            Route::resource('calendar', CalendarController::class);
            Route::resource('blog', BlogController::class);
            Route::resource('contact', ControllersContactController::class);
            Route::resource('discount', DiscountController::class);
        });
        Route::middleware('checkNotCustomer')->group(function () {
            Route::get('/', [MainController::class,'home'])->name('admin');
            Route::post('/', [MainController::class, 'index']);
        });
        Route::resource('class', ClassController::class);
    });
});
