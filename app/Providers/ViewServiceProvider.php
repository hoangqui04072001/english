<?php
 
namespace App\Providers;

use App\View\Composers\CategoryComposer;
use App\View\Composers\CategoryProductComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;


class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        // ...
    }
    public function boot(): void
    {
       
        View::composer('client.layout.category', CategoryComposer::class);
        View::composer('client.product.index', CategoryProductComposer::class);
    }
}