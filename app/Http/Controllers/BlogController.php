<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $blog;

    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }
    public function index()
    {
        $blogs = $this->blog->paginate(5);
        return view('admin.blog.index',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {   
        $validatedData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'categoryblog' => 'required',
            'name' => 'required',
            'image' => 'required',
            'views' => 'integer|min:0',
        ]);
        $title = $request->input('title');
        $description = $request->input('description');
        $categoryblog = $request->input('categoryblog');
        $name = $request->input('name');
        $image = $request->input('image');
        $blog = new Blog( $validatedData);
        $blog->title = $title;
        $blog->description = $description;
        $blog->categoryblog = $categoryblog;
        $blog->name = $name;
        $blog->image = $image;
        $blog->views = 0; 
        // dd($blog);
        $blog->save(); 
        return redirect()->route('blog.index')->with(['message'=>'Tạo lịch học '.$blog->title." thành công"]);

    }
   
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $blog = $this->blog->findOrfail($id);
        return view('admin.blog.edit', compact('blog'));
    
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
      
     $request->validate([
        'title' => 'required',
        'description' => 'required',
        'categoryblog' => 'required',
        'name' => 'required',
        'image' => 'required',
        'views' => 'integer|min:0',
    ]);

    $blog = Blog::find($id);
    $blog->title = $request->input('title');
    $blog->description = $request->input('description');
    $blog->categoryblog = $request->input('categoryblog');
    $blog->name = $request->input('name');

    // Lấy đường dẫn URL từ trường nhập liệu đường dẫn URL (nếu có)
   
    $blog->image = $request->input('image');
   
    $blog->views = 0; 
    // Lưu dữ liệu vào CSDL
    $blog->update(); 

    return redirect()->route('blog.index')->with(['message'=>'Cập nhật lịch học '.$blog->title." thành công"]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Blog::destroy($id);
        return to_route('blog.index')->with(['message'=>'Xóa Thành Công']);
    }
    public function search(Request $request)
    {
        $searchText = $request->input('searchText');
        $blogs = Blog::where('title', 'like', '%' . $searchText . '%')
                    ->orWhere('name', 'like', '%' . $searchText . '%')
                    ->get();

        return view('admin.blog.index', ['blogs' => $blogs]);
    }
}
