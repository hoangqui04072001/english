<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderProduct;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $orders = Order::paginate(5);

        return view('admin.order.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.order.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $orderDetails = OrderProduct::where('order_id', $id)->paginate(5);

        return view('admin.order.show', compact('orderDetails'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Order::destroy($id);
        return to_route('order.index')->with(['message'=>'Xóa Thành Công']);
    }
    public function search(Request $request)
    {
        $searchText = $request->input('searchText');
        $blogs = Order::where('order_name', 'like', '%' . $searchText . '%')
                    ->orWhere('order_phone', 'like', '%' . $searchText . '%')
                    ->get();

        return view('admin.order.index', ['blogs' => $blogs]);
    }
}
