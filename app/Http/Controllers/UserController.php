<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $user;
    protected $role;
    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }
    public function index()
    {
        $users = $this->user->paginate(16);
        return view('admin.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $roles = $this->role->all()->groupBy('name');
        return view('admin.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateUserRequest $request)
    {
        $datacreate =$request->all();
        
        $datacreate['password'] = Hash::make($request->password);

        $user = $this->user->create($datacreate);
        $user->roles()->attach($datacreate['role_id' ?? []]);
        return to_route('user.index')->with(['message'=>'tạo thành công']);
    }

   

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = $this->user->findOrfail($id)->load('roles');
        $roles = $this->role->all()->groupBy('name');
        return view('admin.user.edit', compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, string $id)
    {
        $user = $this->user->findOrfail($id)->load('roles');
        $dataupdate =$request->except('password');
        if($request->password)
        {
            $dataupdate['password'] = Hash::make($request->password);
        }
       
        $user->update($dataupdate);
        $user->roles()->sync($dataupdate['role_id'] ?? []);
        return to_route('user.index')->with(['message'=>'sửa thành công']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
       
        User::destroy($id);
        return to_route('user.index')->with(['message'=>'Xóa Thành Công']);
   
    }
    public function search(Request $request)
    {
        $searchText = $request->input('searchText');
        $blogs = User::where('name', 'like', '%' . $searchText . '%')
                    ->orWhere('phone', 'like', '%' . $searchText . '%')
                    ->get();

        return view('admin.user.index', ['blogs' => $blogs]);
    }
}
