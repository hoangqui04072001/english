<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;

class MainController extends Controller
{

    public function __construct()
    {
    $this->middleware('auth');
    }
    public function home()
    {
        $orderProduct = new OrderProduct();
        $teacher = new Teacher();
        $product = new Product();
        $student = new Student();
        $tongban = $orderProduct->getSoldProductsCount();
        $tongdon = $orderProduct->getNumberProductsCount();
        $tonggv = $teacher->getnumber();
        $tongsp = $product->getnumber();
        $tonghv = $student->getnumber();
        $revenueByMonth = Order::revenueByMonth();
        return view('admin.dashboard.index', compact('tongban','tongdon','tonggv','tongsp','tonghv','revenueByMonth'));
    }
    public function index()
    {
       
   
        return view('admin.dashboard.index');
    }
}
