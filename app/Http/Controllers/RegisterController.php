<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    public function index()
    {
        return view('logins.register');
    }
    public function store(Request $request)
    {
        $this->validate($request,
        [
         'email'=>'required|email:filter',
         'password'=>'required',
         'confirm-password'=>'required',
         'cccd'=>'required',
         'type'=>'required',
        ]);
    //    dd($request->input());
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->cccd = $request->cccd;
        $user->type = $request->type;
        $user->save();
        Session::flash('success','Bạn đã tạo tài khoản thành công');
        return redirect()->back();


    }

}
