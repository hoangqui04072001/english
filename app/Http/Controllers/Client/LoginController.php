<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function login()
    {
        return view('client.loginclient');
    }
    public function register()
    {
        return view('client.registerclient');
    }

    public function forget()
    {
        return view('client.forgetclient');
    }
    public function store_forget(Request $request)
    {
        
    }
    /**
     * Show the form for creating a new resource.
     */
    public function store_login(Request $request)
    {
        // dd($request->input());
       
        $this->validate($request, [
            'email' => 'required|email:filter',
            'password' => 'required'
        ], [
            'email.required' => 'Vui lòng nhập địa chỉ email.',
            'email.email' => 'Địa chỉ email không hợp lệ.',
            'password.required' => 'Vui lòng nhập mật khẩu.'
        ]);
       if(Auth::attempt(
        [
            'email'=>$request->input('email'),
            'password'=>$request->input('password')
        ],  $request->input('remember'))){
              return redirect()->route('home');
        }
        Session::flash('error','Email hoặc Password không chính xác');
         return redirect()->back();
}


    /**
     * Store a newly created resource in storage.
     */
    public function store_register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email:filter',
            'password' => 'required|min:6',
            'confirm-password' => 'required|same:password',
            'cccd' => 'required|numeric',
            
        ], [
            'name.required'=>'Vui lòng nhập tên của mình.',
            'email.required' => 'Vui lòng nhập địa chỉ email.',
            'email.email' => 'Địa chỉ email không hợp lệ.',
            'password.required' => 'Vui lòng nhập mật khẩu.',
            'confirm-password.required' => 'Vui lòng nhập lại mật khẩu.',
            'confirm-password.same' => 'Mật khẩu nhập lại không giống với mật khẩu.',
            'cccd.required' => 'Vui lòng nhập số CCCD.',
            'cccd.numeric' => 'Số CCCD phải là số.',
            
            
        ]);
    //    dd($request->input());
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->cccd = $request->cccd;
        $user->type = $request->type;
        $user->save();
        Session::flash('success','Bạn đã tạo tài khoản thành công');
        return redirect()->back();
    }
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    } 
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('client.login');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
