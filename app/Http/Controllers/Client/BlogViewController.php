<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogViewController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $blog;
 
     public function __construct(Blog $blog)
     {
        $this->blog = $blog;
   
     }
    public function index()
    {
        $blogs = Blog::query()->paginate(3);
        return view('client.blog.blog', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function blog_details(Request $request,$id)
    {
        $blog = Blog::findOrFail($id);
        $blogs = Blog::all();
        $blog->views += 1;
        $blog->save();
        $textareaContent = $blog->description;
        return view('client.blog.details',compact('blog','textareaContent','blogs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
    public function sreach(Request $request) {
        $searchInput = $request->input('searchInput');
        $results = Blog::where('title', 'like', '%' . $searchInput . '%')->get();
        return response()->json($results);
    }
}
