<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Classes;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\VideoProduct;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::where('user_id', Auth::user()->id)->orderByDesc('created_at')->get();

        return view('client.orders', compact('orders'));
    }

    public function show($id)
    {
        $orderDetails = OrderProduct::where('order_id', $id)->get();

        return view('client.order_detail', compact('orderDetails'));
    }

    public function showDetailProduct($id)
{
    $classes = Classes::where('product_id', $id)->paginate(3);
    $videoProducts = VideoProduct::where('products_id', $id)->paginate(3);

    if ($videoProducts->count() > 0) {
        // Nếu có dữ liệu trong $videoProducts, thực hiện chuyển hướng đến trang product
        return view('client.product_detail', compact('videoProducts'));
    } else {
        // Nếu không có dữ liệu trong $videoProducts, thực hiện chuyển hướng đến trang class
        return view('client.order_detail_class', compact('classes'));
    }
}
}
