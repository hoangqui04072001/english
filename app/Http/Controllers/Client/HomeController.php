<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     protected $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }
    public function index()
    {
        $products = $this->product->first('id')->paginate(8);
        return view('client.layout.app',compact('products'));
    }

    public function cart()
    {
       
        return view('client.cart');
    }

    public function product()
    { 
        // $product = $this->product->latest('id')->paginate(3);
        return view('client.product.index');
    
    }
   
}
