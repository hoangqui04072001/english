<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function email()
    {
       return view('client.email');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function checkemail()
    {   
            $name ='Nguyễn Hoàng Quý';
            Mail::send('client.email_check',compact('name'),
            function ($email) use($name){
            $email->to('hoangqui04072001@gmail.com','Hoàng Quý');
            $email->subject('Khóa Học Tiếng Anh Karma');
        });
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
