<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Support\Facades\Auth;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Models\Product;
use AmrShawky\Currency;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class CheckOutController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('client.checkout');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function pay(Request $request)
    {
        $product = [];
        $orderId = 'order_' . random_int(100000, 999999);
        $amount = (int) array_reduce(
            session('cart'),
            function ($total, $item) {
                $total += $item['sell'] * $item['qty'];

                return $total;
            },
            0
        );
        Order::create([
            'id' => $orderId,
            'user_id' => Auth::user()->id,
            'order_name' => $request->order_name,
            'order_gender' => $request->order_gender,
            'order_phone' => $request->order_number,
            'order_email' => $request->order_email,
            'total' => $amount,
            'status' => 1
        ]);

        foreach (session('cart') as $k => $v) {
            OrderProduct::create([
                'sellnumber' => $v['qty'],
                'sell' => (int) $v['sell'],
                'order_id' => $orderId,
                'product_id' => $k
            ]);
        }

        foreach (session('cart') as $k => $v) {
            $productDetail = Product::find($k); 
            $product['items'][] = [
                'name' => $productDetail->name,
                'price' => round(Currency::convert()
                ->from('VND')
                ->to('USD')
                ->amount($v['sell'])
                ->get(), 2),
                'qty' => $v['qty']
            ];
        }

        $product['invoice_id'] = $orderId;
        $product['invoice_description'] = "Pay successful, you get the new Order#{$orderId}";
        $product['return_url'] = route('done.payment.paypal', ['id' => $orderId]);
        $product['cancel_url'] = route('cancel.payment.paypal');
        $product['total'] = round(Currency::convert()
        ->from('VND')
        ->to('USD')
        ->amount($amount)
        ->get(), 2);
        
        $paypalModule = new ExpressCheckout;

        $res = $paypalModule->setExpressCheckout($product);
        $res = $paypalModule->setExpressCheckout($product, true);
        

        return redirect($res['paypal_link']);
    }

    public function cancelPaymentPaypal()
    {
        toastr()->error('Bạn đã hủy thanh toán !');

        return redirect()->route('check.out');
    }
  
    public function donePaymentPaypal(Request $request, $id)
    {
        $paypalModule = new ExpressCheckout;
        $response = $paypalModule->getExpressCheckoutDetails($request->token);
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            $order = Order::findOrFail($id);
            $cart = session('cart');
            Mail::send('client.email_check',compact('order','cart'),
            function ($email) use($order){
            $email->to( $order->order_email,$order->order_name);
            $email->subject('Khóa Học Tiếng Anh Karma');
        });
            session()->forget('cart');
            toastr()->success('Thanh toán thành công');

            return redirect()->route('client.mail');
        }
    }
}
