<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Discount;
use App\Models\Product;
use App\Models\Review;
use Illuminate\Http\Request;

class ClientProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    protected $product;
    protected $comment;
    public function __construct(Product $product, Comment $comment)
    {
        $this->product = $product;
        $this->comment = $comment;
    }
    public function home(Request $request)
    {
        $startPrice = $request->input('start_price');
        $endPrice = $request->input('end_price');
        if (isset($_GET['start_price']) && $_GET['end_price']) {
            $products = Product::whereBetween('sell', [$startPrice, $endPrice])
                ->orderBy('sell', 'ASC')
                ->paginate(12)
                ->appends(request()->query());
        } else {
            $products = Product::query()->paginate(12);
        }
        return view('client.product.index', compact('products'));
    }
    public function homesort(Request $request)
    {
        // dd($request);

        if (isset($request->name_slug)) {
            $category_id = Category::where('name_slug', $request->name_slug)->first();
            $tenloai = $category_id->tenloai;

            if ($request->sapxep == 'date') {
                // Mới nhất
                $products = Product::where('category_id', $category_id->id)
                    ->orderBy('created_at', 'desc')
                    ->paginate(3);

                // Ghi vào SESSION
                session()->put('sapxep', 'date');
            } elseif ($request->sapxep == 'price') {
                // Xếp theo giá: thấp đến cao
                $products = Product::where('category_id', $category_id->id)
                    ->orderBy('sell', 'asc')
                    ->paginate(3);

                //products Ghi vào SESSION
                session()->put('sapxep', 'price');
            } elseif ($request->sapxep == 'price-desc') {
                // Xếp theo giá: cao xuống thấp
                $products = Product::where('category_id', $category_id->id)
                    ->orderBy('sell', 'desc')
                    ->paginate(3);

                // Ghi vào SESSION
                session()->put('sapxep', 'price-desc');
            }
            // Mặc định
            else {
                $products = Product::where('category_id', $category_id->id)->paginate(16);

                // Ghi vào SESSION
                session()->put('sapxep', 'default');
            }

            return view('client.product.index', compact('products', 'tenloai'));
        } else {
            if ($request->sapxep == 'date') {
                // Mới nhất
                $products = Product::orderBy('created_at', 'desc')->paginate(16);

                // Ghi vào SESSION
                session()->put('sapxep', 'date');
            } elseif ($request->sapxep == 'price') {
                // Xếp theo giá: thấp đến cao
                $products = Product::orderBy('sell', 'asc')->paginate(16);

                // Ghi vào SESSION
                session()->put('sapxep', 'price');
            } elseif ($request->sapxep == 'price-desc') {
                // Xếp theo giá: cao xuống thấp
                $products = Product::orderBy('sell', 'desc')->paginate(16);

                // Ghi vào SESSION
                session()->put('sapxep', 'price-desc');
            }
            // Mặc định
            else {
                $products = Product::paginate(16);

                // Ghi vào SESSION
                session()->put('sapxep', 'default');
            }

            return view('client.product.index', compact('products'));
        }
    }

    public function index(Request $request, $category_id)
    {
        $products = Product::query()
            ->where('category_id', $category_id)
            ->paginate(3);
        return view('client.product.index', compact('products'));
    }
    public function addtocart(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $cart = session()->get('cart', []);
        if (isset($cart[$id])) {
            $cart[$id]['qty']++;
        } else {
            $cart[$id] = [
                'name' => $product->name,
                'picture' => $product->picture,
                'sell' => $product->sell,
                'qty' => 1,
            ];
        }
        session()->put('cart', $cart);
        return redirect()
            ->back()
            ->with('success', 'thêm khóa học vào giỏ hàng thành công');

        // return view('client.product.index', compact('products'));
    }
    /**
     * Show the form for creating a new resource.
     */
    public function product_details(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $reviews = Review::where('product_id', $id)->get();
        $comments = Comment::where('product_id', $id)->get();

        $averageRating = Review::where('product_id', $id)->avg('rating');
        $averageRating = round($averageRating, 1);
        $product->average_rating = $averageRating;
        $oneStarCount = Review::where('product_id', $id)
            ->where('rating', 1)
            ->count();
        $twoStarCount = Review::where('product_id', $id)
            ->where('rating', 2)
            ->count();
        $threeStarCount = Review::where('product_id', $id)
            ->where('rating', 3)
            ->count();
        $fourStarCount = Review::where('product_id', $id)
            ->where('rating', 4)
            ->count();
        $fiveStarCount = Review::where('product_id', $id)
            ->where('rating', 5)
            ->count();

        return view('client.product.details', compact('product', 'comments', 'reviews', 'averageRating', 'oneStarCount', 'twoStarCount', 'threeStarCount', 'fourStarCount', 'fiveStarCount'))->with('success', 'Đánh giá của bạn đã được gửi thành công!');
    }
    public function reviews(Request $request)
    {
        $validatedData = $request->validate([
            'rating' => 'required|integer|min:1|max:5',
            'name' => 'required|string|max:255',
            'review' => 'required|string',
            'product_id' => 'required|integer|exists:products,id',
        ]);

        $name = $request->input('name');
        $review = $request->input('review');
        $rating = $request->input('rating');

        $reviewObject = new Review($validatedData);
        $reviewObject->name = $name;
        $reviewObject->review = $review;
        $reviewObject->rating = $rating;
        $reviewObject->save();
        session()->put('rating', $rating);

        return redirect()
            ->back()
            ->with('success', 'Đánh giá của bạn đã được gửi thành công!');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function comment(Request $request)
    {
        try {
            $comment_product_id = $request->comment_product_id;
            $comment_product_name = $request->comment_product_name;
            $comment_product_email = $request->comment_product_email;
            $comment_product_phone = $request->comment_product_phone;
            $comment_product_comment = $request->comment_product_comment;
            $comment = new Comment();
            $comment->product_id = $comment_product_id;
            $comment->name = $comment_product_name;
            $comment->email = $comment_product_email;
            $comment->phone = $comment_product_phone;
            $comment->comment = $comment_product_comment;

            $comment->save();

            return response()->json(['message' => 'Comment đăng thành công']);
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function filterByPrice(Request $request)
    {
        $sell = $request->input('sell'); // Giá tiền để lọc ra
        $products = Product::where('sell', '<=', $sell)->get(); // Lọc sản phẩm với giá tiền nhỏ hơn hoặc bằng giá tiền được cung cấp
        return view('client.product.index', compact('products')); // Trả về view hiển thị danh sách sản phẩm đã lọc
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        if ($request->id && $request->qty) {
            if (session()->has('cart')) {
                $cart = session()->get('cart');
                $cart[$request->id]['qty'] = $request->qty;
                session()->put('cart', $cart);
            } else {
                // Khởi tạo giỏ hàng nếu chưa tồn tại
                $cart = [$request->id => ['qty' => $request->qty]];
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Cập nhật số lượng sản phẩm thành công!');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function remove(Request $request)
    {
        if ($request->id) {
            // dd($request->id);
            $cart = session()->get('cart');
            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Khóa học đã được xóa thành công !');
        }
    }
    public function search(Request $request)
    {
        
        $searchTerm = $request->get('q'); // Lấy giá trị tìm kiếm từ request

        // Thực hiện tìm kiếm sản phẩm theo từ khóa
        $products = Product::where('name', 'like', '%' . $searchTerm . '%')->get();

        $output = '<ul>';
        if ($products->count() > 0) {
            foreach ($products as $product) {
                $output .= '<li><a href="' . route('client.product.details', ['id' => $product->id]) . '">' . $product->name . '</a></li>'; // Thêm mỗi sản phẩm vào danh sách li
            }
        } else {
            $output .= '<li>Không tìm thấy sản phẩm.</li>';
        }
        $output .= '</ul>';

        return response()->json(['data' => $output]);
    }
}
