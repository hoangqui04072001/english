<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Teacher;
use App\Models\VideoProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Collection;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    
    protected $product;
    public function __construct(Product $product)
    {
        
        $this->product = $product;
    }
    public function index()
    {
       
        $products = $this->product->paginate(5);
        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $category = Category::all();
        $teacher = Teacher::all();
        return view('admin.product.create',compact('category','teacher'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateProductRequest $request)
    {
    //  dd($request->input());
        $createdata = $request->all();
        $filename = time().$request->file('picture')->getClientOriginalName();
        $path = $request->file('picture')->storeAs('images', $filename, 'public');
        $createdata['picture'] = '/storage/' .$path;
        $createdata['name_slug'] = Str::slug($request->name, '-');
        Product::create($createdata);
        return redirect()->route('product.index')->with(['message'=>'thêm khóa học thành công']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = Product::findOrFail($id);
        $videos = $product->VideoProduct()->paginate(10);
        return view('admin.product.show', compact('product', 'videos'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $category = Category::all();
        $teacher = Teacher::all();
        $product = Product::find($id);
        return view('admin.product.edit', compact('category','teacher','product'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, string $id)
    {
        // dd($request->input());
        $updatedata = $request->all();
        $product = $this->product->findOrFail($id);
        if ($request->hasFile('picture')) {
            // Tạo chuỗi tên tệp ảnh mới
            $filename = time().$request->file('picture')->getClientOriginalName();
        
            // Lưu trữ tệp ảnh mới
            $path = $request->file('picture')->storeAs('images', $filename, 'public');
        
            // Cập nhật đường dẫn đến tệp ảnh mới trong cơ sở dữ liệu
            $updatedata['picture'] = '/storage/'.$path;
            
        }
        $updatedata['name_slug'] = Str::slug($request->name, '-');
        $product->update($updatedata);
        return redirect()->route('product.index')->with(['message'=>'Sửa khóa học '.$product->name .' thành công']);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Product::destroy($id);
        return to_route('product.index')->with(['message'=>'Xóa Thành Công']);
    }
    public function search(Request $request)
    {
        $searchText = $request->input('searchText');
        $blogs = Product::where('name', 'like', '%' . $searchText . '%')->get();

        return view('admin.product.index', ['blogs' => $blogs]);
    }
}
