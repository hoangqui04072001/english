<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CreateCaterogyRequest;
use App\Http\Requests\Category\UpdateCaterogyRequest;
use App\Http\Requests\User\CreateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }
    public function index()
    {
        $categories = $this->category->paginate(5);
        return view('admin.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = $this->category->all()->groupBy('name');
        return view('admin.category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateCaterogyRequest $request)
    { 
        // dd($request->all());
        $data = $request->all();
        $category = Category::create($data);
        return redirect()->route('category.index')->with(['message'=>'Tạo '.$category->name." thành công"]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        
        $category = $this->category->findOrfail($id);
        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCaterogyRequest $request, string $id)
    {
        $dataupdate = $request->all();
        $category = $this->category->findOrFail($id);
        $category->update($dataupdate);
        return redirect()->route('category.index')->with(['message'=>'Sửa khóa học '.$category->name .' thành công']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Category::destroy($id);
        return to_route('category.index')->with(['message'=>'Xóa Thành Công']);
   
    }
    public function search(Request $request)
    {
        $searchText = $request->input('searchText');
        $blogs = Category::where('name', 'like', '%' . $searchText . '%')->get();

        return view('admin.category.index', ['blogs' => $blogs]);
    }
}
