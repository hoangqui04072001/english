<?php

namespace App\Http\Controllers;

use App\Models\Discount;
use App\Models\Product;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    protected $discount;

    public function __construct(Discount $discount)
    {
        $this->discount = $discount;
    }
    public function index()
    {
        $discounts = $this->discount->paginate(5);
        return view('admin.discount.index',compact('discounts'));
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $product =Product::all();
        return view('admin.discount.create', compact('product'));

    /**
     * Store a newly created resource in storage.
     */
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|unique:discounts,product_id',
            'discount' => 'required',
        ], 
        [
            'product_id.unique' => 'Khóa học đã có mã giảm giá.',
        ]);
       
        
        $discount = new Discount();
        $discount->product_id = $request->input('product_id');
        $discount->discount = $request->input('discount');
        $discount->save();

      
        return redirect()->route('discount.index')->with('success', 'tạo thành công!');
    
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $discount = Discount::findOrFail($id);
        $product = Product::all();
        return view('admin.discount.edit',compact('product','discount'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'product_id' => 'required',
            'discount' => 'required',
        ]);
        $discount = Discount::findOrFail($id);
        $discount->product_id = $request->input('product_id');
        $discount->discount = $request->input('discount');
        $discount->update();
        return redirect()->route('discount.index')->with('success', 'sửa thành công!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Discount::destroy($id);
        return to_route('discount.index')->with(['message'=>'Xóa Thành Công']);
    }
    public function search(Request $request)
    {
        $searchText = $request->input('searchText');
        $blogs = Discount::where('product_id', 'like', '%' . $searchText . '%')
                    ->orWhere('discount', 'like', '%' . $searchText . '%')
                    ->get();

        return view('admin.discount.index', ['blogs' => $blogs]);
    }
}
