<?php

namespace App\Http\Controllers;

use App\Http\Requests\Calendar\CreateCalendarRequest;
use App\Models\Calendar;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $calendar;

    public function __construct(Calendar $calendar)
    {
        $this->calendar = $calendar;
    }
    public function index()
    {
        $calendars = $this->calendar->paginate(5);
        return view('admin.calendar.index',compact('calendars'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $calendars = $this->calendar->all();
        return view('admin.calendar.create',compact('calendars'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateCalendarRequest $request)
    {
        // dd($request->all());
        $data = $request->all();
        $calendar = Calendar::create($data);
        return redirect()->route('calendar.index')->with(['message'=>'Tạo lịch học '.$calendar->lesson." thành công"]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $calendar = $this->calendar->findOrfail($id);
        return view('admin.calendar.edit', compact('calendar'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        // dd($request->all());
        $dataupdate = $request->all();
        $calendar = $this->calendar->findOrFail($id);
        $calendar->update($dataupdate);
        return redirect()->route('calendar.index')->with(['message'=>'Sửa lịch học '.$calendar->lesson .' thành công']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Calendar::destroy($id);
        return to_route('calendar.index')->with(['message'=>'Xóa Thành Công']);
    }
    public function search(Request $request)
    {
        $searchText = $request->input('searchText');
        $blogs = Calendar::where('lesson', 'like', '%' . $searchText . '%')
                    ->get();

        return view('admin.calendar.index', ['blogs' => $blogs]);
    }
}
