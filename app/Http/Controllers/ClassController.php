<?php

namespace App\Http\Controllers;

use App\Http\Requests\Classes\CreateClassRequest;
use App\Http\Requests\Classes\UpdateClassRequest;
use App\Models\Calendar;
use App\Models\Classes;
use App\Models\Product;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Auth;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $class;

    public function __construct(Classes $class)
    {
        $this->class = $class;
    }
    public function index()
    {
        $classes = $this->class->paginate(5);
        return view('admin.class.index',compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {   
        if (Auth::user()->hasRole('Admin')) {
            $teacher = Teacher::all();
            $product = Product::all();
            $calendar = Calendar::all();
            return view('admin.class.create', compact('teacher','product','calendar'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateClassRequest $request)
    {
        // dd($request->input());
        $datacreate = $request->all();
        $class = $this->class->create($datacreate);
        return redirect()->route('class.index')->with(['message'=>'tạo lớp: '.$class->name.' thành công']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $class = Classes::find($id);
        $teacher = Teacher::all();
        $product = Product::all();
        $calendar = Calendar::all();
        return view('admin.class.edit',compact('class','teacher','product','calendar'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateClassRequest $request, string $id)
    {
        // dd($request->input());
        $dataupdate = $request->all();
        $class = $this->class->findOrFail($id);
        $class->update($dataupdate);
        return redirect()->route('class.index')->with(['message'=>'Sửa lớp '.$class->name .' thành công']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Classes::destroy($id);
        return to_route('class.index')->with(['message'=>'Xóa Thành Công']);
    
    }
    public function search(Request $request)
    {
        $searchText = $request->input('searchText');
        $blogs = Classes::where('name', 'like', '%' . $searchText . '%')
                    ->orWhere('code', 'like', '%' . $searchText . '%')
                    ->get();

        return view('admin.class.index', ['blogs' => $blogs]);
    }
}
