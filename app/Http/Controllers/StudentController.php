<?php

namespace App\Http\Controllers;

use App\Http\Requests\Student\CreateStudentRequest;
use App\Http\Requests\Student\UpdateStudentRequest;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }
    public function index()
    {
        $students = $this->student->paginate(5);
        return view('admin.student.index',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $useremail = User::all();
        return view('admin.student.create',compact('useremail'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateStudentRequest $request)
    {
        
        $datacreate = $request->all();
        $student = $this->student->create($datacreate);
        return redirect()->route('student.index')->with(['message'=>'tạo học viên: '.$student->name.' thành công']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {   
        $student = Student::find($id);
        $useremail = User::all();
        return view('admin.student.edit',compact('student','useremail'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateStudentRequest $request, string $id)
    {
        $dataupdate = $request->all();
        $student = $this->student->findOrFail($id);
        $student->update($dataupdate);
        return redirect()->route('student.index')->with(['message'=>'Sửa học viên '.$student->name .' thành công']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Student::destroy($id);
        return to_route('student.index')->with(['message'=>'Xóa Thành Công']);
    }
    public function search(Request $request)
    {
        $searchText = $request->input('searchText');
        $blogs = Student::where('name', 'like', '%' . $searchText . '%')
                    ->orWhere('phone', 'like', '%' . $searchText . '%')
                    ->get();

        return view('admin.student.index', ['blogs' => $blogs]);
    }
}
