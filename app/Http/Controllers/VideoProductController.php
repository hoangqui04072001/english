<?php

namespace App\Http\Controllers;

use App\Http\Requests\Video\CreateVideoRequest;
use App\Http\Requests\Video\UpdateVideoRequest;
use App\Models\Product;
use App\Models\VideoProduct;
use Illuminate\Http\Request;

class VideoProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $video;

    public function __construct(VideoProduct $video)
    {
        $this->video = $video;
    }
    public function index()
    {
        $videos = $this->video->paginate(5);
        return view('admin.videoproduct.index',compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $product = Product::all();
        return view('admin.videoproduct.create',compact('product'));
    
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateVideoRequest $request)
    {
        $datacreate = $request->all();
        if ($request->hasFile('video')) {
            $filevideo = $request->file('video');
            $filevideo->move('upload',$filevideo->getClientOriginalName());
            $file = $filevideo->getClientOriginalName();
            $datacreate['video'] = $file;
        }
        $products_id = $datacreate['products_id'] ?? Product::first()->id;
        VideoProduct::create([
        'name' => $datacreate['name'],
        'video' => $datacreate['video'],
        'products_id' => $products_id,
    ]);
    return redirect()->route('videoproduct.index')->with(['message'=>'thêm video khóa học thành công']);
}

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $video = VideoProduct::find($id);
        $product = Product::all();
        return view('admin.videoproduct.edit',compact('product','video'));
    
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateVideoRequest $request, string $id)
    {
        $dataupdate = $request->all();
        $video = VideoProduct::findOrFail($id);

        // Nếu có tệp được tải lên
        if ($request->hasFile('video')) {
            $filevideo = $request->file('video');
            $filevideo->move('upload', $filevideo->getClientOriginalName());
            $file = $filevideo->getClientOriginalName();
            $dataupdate['video'] = $file;
        } else {
            // Không tải tệp lên, sử dụng giá trị cũ
            $dataupdate['video'] = $video->video;
        }

        // Lấy giá trị của products_id từ request, nếu không có thì lấy giá trị cũ
        $products_id = $dataupdate['products_id'] ?? $video->products_id;

        $video->update([
            'name' => $dataupdate['name'],
            'video' => $dataupdate['video'],
            'products_id' => $products_id,
        ]);

        return redirect()->route('videoproduct.index')->with(['message' => 'Cập nhật video khóa học thành công']);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $video = VideoProduct::find($id);
        if(!$video) {
            return redirect()->back()->with(['message' => 'Video không tồn tại']);
        }
        $video->delete();
        return redirect()->back()->with(['message' => 'Xóa video khóa học thành công']);
    }
}
