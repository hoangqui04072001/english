<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginUserController extends Controller
{
    public function index()
    {
        return view('logins.login');
    }
    public function store(Request $request)
    {
        // dd($request->input());
       
       $this->validate($request,
       [
        'email'=>'required|email:filter',
        'password'=>'required'
       ]);
       if(Auth::attempt(
        [
            'email'=>$request->input('email'),
            'password'=>$request->input('password')
        ],$request->input('remember'))){
              return redirect()->route('admin');
    }
     Session::flash('error','Email hoặc Password không chính xác');
   return redirect()->back();
}

 /**
     * Display the specified resource.
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('loginUser');
    }
}
