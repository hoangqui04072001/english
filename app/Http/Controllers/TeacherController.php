<?php

namespace App\Http\Controllers;

use App\Http\Requests\Teacher\CreateTeacherRequest;
use App\Http\Requests\Teacher\UpdateTeacherRequest;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $teacher;
    public function __construct(Teacher $teacher)
    {
        $this->teacher = $teacher;
    }
    public function index()
    {
        $teachers = $this->teacher->paginate(5);
        return view('admin.teacher.index',compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $useremail = User::all();
        return view('admin.teacher.create',compact('useremail'));
    
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateTeacherRequest $request)
    {
    //    dd($request->all());
        $datacreate = $request->all();
        $teacher = $this->teacher->create($datacreate);
        return redirect()->route('teacher.index')->with(['message'=>'tạo giáo viên: '.$teacher->name.' thành công']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $teacher = Teacher::find($id);
        $useremail = User::all();
        return view('admin.teacher.edit',compact('teacher','useremail'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTeacherRequest $request, string $id)
    {
        // dd($request->all());
        $dataupdate = $request->all();
        $teacher = $this->teacher->findOrFail($id);
        $teacher->update($dataupdate);
        return redirect()->route('teacher.index')->with(['message'=>'Sửa giáo viên '.$teacher->name .' thành công']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Teacher::destroy($id);
        return to_route('teacher.index')->with(['message'=>'Xóa Thành Công']);
    
    }
    public function search(Request $request)
    {
        $searchText = $request->input('searchText');
        $blogs = Teacher::where('name', 'like', '%' . $searchText . '%')
                    ->orWhere('phone', 'like', '%' . $searchText . '%')
                    ->get();

        return view('admin.teacher.index', ['blogs' => $blogs]);
    }
}
