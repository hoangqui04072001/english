<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class CreateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required',
            'name' => 'required',
            'code' => ['required', 'regex:/^HV\d{4}$/', 'unique:students,code'],
            'phone' => ['required', 'unique:students,phone'],
            'email' => ['required', 'unique:students,email'],
            'birth_date' => 'required',
            'gender' => 'required',
            'created_by' => 'required',
            'updated_by' => 'required',
        ];
    }
}
