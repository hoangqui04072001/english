<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required', 
            'name'=> 'required',
            'code' => ['required', 'regex:/^HV\d{4}$/'],
            'phone' => 'required',
            'email'=>  'required|email',
            'birth_date'=>'required',
            'gender'=>'required',            
        ];
    }
}
