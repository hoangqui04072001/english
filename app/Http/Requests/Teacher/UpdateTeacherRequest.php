<?php

namespace App\Http\Requests\Teacher;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'user_id'=>'required',
            'name'=>'required',
            'email'=>'required',
            'address'=>'required',
            'birth_date'=>'required',
            'status'=>'required',
            'phone'=>'required',
            'level'=>'required',
            'created_by'=>'required',
            'updated_by'=>'required'
        ];
    }
}
