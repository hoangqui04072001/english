<?php

namespace App\Http\Requests\Video;

use Illuminate\Foundation\Http\FormRequest;

class CreateVideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        // dd(request()->video->getClientMimeType() == 'video/mp4');
        return [
            'name'=> 'required',
            'video' => 'required|mimetypes:video/mp4,video/mkv',
            'products_id' => 'required'
           
        ]; // |mimes:mp4,ogx,oga,ogv,ogg,avi,flv' , |unique:products,name cai nay viet sai nhe
    }
}
