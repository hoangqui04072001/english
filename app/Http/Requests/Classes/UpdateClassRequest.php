<?php

namespace App\Http\Requests\Classes;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'=>'required',
            'code'=>'required',
            'status'=>'required',
            'description'=>'required',
            'sessions_number'=>['required', 'numeric', 'min:1', 'max:100'],
            'calendar_id'=>'required',
            'teacher_id'=>'required',
            'product_id'=>'required',
            'quantity'=>['required', 'numeric', 'min:1', 'max:20'],
            'start_date'=>'required|date',
            'end_date'=>'required|date|after:start_date',
            'created_by'=>'required',
            'updated_by'=>'required',
        ];
    }
    public function messages(): array
    {
        return [
            'quantity.max' => 'Số lượng không được lớn hơn 40.',
            'quantity.required' => 'Số lượng là trường bắt buộc.',
            'quantity.min' => 'Số lượng không được nhỏ hơn 1.',
            'quantity.numeric' => 'Số lượng phải là một số.',
            'sessions_number.min' => 'Số buổi học phải lớn hơn hoặc bằng 1.',
            'sessions_number.max' => 'Số buổi học phải nhỏ hơn hoặc bằng 100.',
            'sessions_number.required' => 'Số buổi học là trường bắt buộc.',
            'sessions_number.numeric' => 'Số buổi học phải là một số.',
            'end_date.after' => 'Ngày kết thúc phải sau ngày bắt đầu.',
        
        ];
       
    }
    
}
