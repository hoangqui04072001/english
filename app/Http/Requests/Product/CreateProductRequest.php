<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'=>'required',
            'category_id'=>'required',
            'teacher_id'=>'required',
            'description'=>'required',
            'sell'=>'required',
            'number'=>'required',
            'picture'=>'required',
            'created_by'=>'required',
            'updated_by'=>'required'
        ];
    }
}
