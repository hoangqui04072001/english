<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;

class Authenticate extends Middleware
{
    public function handle($request, Closure $next, ...$guards)
    {
    
        if (Auth::check()) {
            $user = Auth::user();

            if ($user) {
                return $next($request);
            }
            return redirect()->route('loginUser');
        } else {
            return redirect()->route('loginUser');
        }
    }
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
        return $request->expectsJson() ? null : route('loginUser');
    }
}
