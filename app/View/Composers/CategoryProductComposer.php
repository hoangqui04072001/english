<?php
 
namespace App\View\Composers;

use App\Models\Category;
use App\Repositories\UserRepository;
use Illuminate\View\View;
 
class CategoryProductComposer
{
    /**
     * Create a new profile composer.
     */
    public function __construct( )
     {}
 
    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
       
       $category = Category::select('id','name')->where('status',1)->orderBy('id')->get();
       $view->with('category', $category);
      
    }
}