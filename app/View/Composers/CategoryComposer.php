<?php
 
namespace App\View\Composers;

use App\Models\Category;
use App\Repositories\UserRepository;
use Illuminate\View\View;
 
class CategoryComposer
{
    /**
     * Create a new profile composer.
     */
    public function __construct( )
     {}
 
    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
       
       $categories = Category::select('id','name')->where('status',1)->orderByDesc('id')->get();
       $view->with('categories', $categories);
      
    }
}