<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Classes extends Model
{
    use HasFactory;

    protected $table ='classes';

    protected $fillable =[
        'name',
        'code',
        'status',
        'description',
        'sessions_number',
        'calendar_id',
        'teacher_id',
        'product_id',
        'quantity',
        'start_date',
        'end_date',
        'created_by',
        'updated_by'

    ];
    public function Teacher():BelongsTo
	{
		return $this->BelongsTo(Teacher::class, 'teacher_id', 'id');
	}
    public function Product():BelongsTo
	{
		return $this->BelongsTo(Product::class, 'product_id', 'id');
	}
    public function Calendar():BelongsTo
	{
		return $this->BelongsTo(Calendar::class, 'calendar_id', 'id');
	}
}
