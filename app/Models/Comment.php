<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{
    use HasFactory;
    protected $table ='comment';

    protected $fillable =[
        'name',
        'product_id',
        'phone',
        'email',
        'comment',

    ];
    public function Product():BelongsTo
	{
		return $this->BelongsTo(Product::class, 'product_id', 'id');
	}
}
