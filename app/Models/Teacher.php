<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Teacher extends Model
{
    use HasFactory;

    protected $table ='teachers';

    protected $fillable =[
        'user_id',
        'name',
        'email',
        'address',
        'birth_date',
        'status',
        'phone',
        'level',
        'created_by',
        'updated_by'
    ];

    public function User():BelongsTo
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function getnumber()
    {
        return $this->count();
    }
}
