<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;
    protected $table = 'categories';

    protected $fillable = [
        'name',
        'name_slug',
        'status',
        'description',
        'created_by',
        'update_by'
    ];
    public function Product():HasMany
	{
		return $this->HasMany(Product::class, 'category_id', 'id');
	}
    
}
