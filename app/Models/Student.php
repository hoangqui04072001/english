<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Student extends Model
{
    use HasFactory;

    protected $table = 'students';

    protected $fillable = [
        'user_id',
        'name',
        'code',
        'phone',
        'email',
        'birth_date',
        'gender',
        'created_by',
        'updated_by'
    ];
    public function user():BelongsTo
    {
        return $this->BelongsTo(User::class,'user_id','id');
    }
    public function getuseremail()
    {
        return optional($this->user)->email;
    }
    public function getnumber()
    {
        return $this->count();
    }
}
