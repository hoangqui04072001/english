<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory;
    protected $table = 'orders';

    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $fillable = [
        'id',
        'status',
        'order_name',
        'order_phone',
        'order_email',
        'order_gender',
        'user_id',
        'total',
    ];
    public function User():BelongsTo
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}
    public static function revenueByMonth()
    {
        return static::selectRaw('YEAR(created_at) AS year, MONTH(created_at) AS month, SUM(total) AS revenue')
            ->groupByRaw('YEAR(created_at), MONTH(created_at)')
            ->orderByRaw('YEAR(created_at) DESC, MONTH(created_at) DESC')
            ->get();
    }
}
