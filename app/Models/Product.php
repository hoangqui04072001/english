<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';

    protected $fillable = [
        'name',
        'name_slug',
        'category_id',
        'teacher_id',
        'description',
        'sell',
        'number',
        'picture',
        'created_by',
        'updated_by'
    ];
    public function Category():BelongsTo
	{
		return $this->belongsTo(Category::class, 'category_id', 'id');
	}
    public function Teacher():BelongsTo
	{
		return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
	
    }public function VideoProduct():HasMany
	{
		return $this->HasMany(VideoProduct::class, 'products_id', 'id');
	}
    public function discounts(): HasOne
    {
        return $this->hasOne(Discount::class, 'product_id', 'id');
    }
    public function reviews():HasMany
    {
        return $this->hasMany(Review::class,'products_id', 'id');
    }
    public function getnumber()
    {
        return $this->count();
    }
   
}
