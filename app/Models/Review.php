<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Review extends Model
{
    use HasFactory;
    protected $table ='reviews';
    protected $fillable = [
        'rating', 
        'name', 
        'review', 
        'product_id'
    ];
    public function Product():BelongsTo
    {
        return $this->belongsTo(Product::class,'product_id', 'id');
    }
}
