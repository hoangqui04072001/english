<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderProduct extends Model
{
    use HasFactory;

    protected $table = 'order_products';

    protected $fillable = [
        'order_id',
        'product_id',
        'sellnumber',
        'sell'
 
    ];
    public function Order():BelongsTo
	{
		return $this->belongsTo(Order::class, 'order_id', 'id');
	}
	
	public function Product():BelongsTo
	{
		return $this->belongsTo(Product::class, 'product_id', 'id');
	}
    public function getNumberProductsCount()
    {
        return $this->sum('sellnumber');
    }
    public function getSoldProductsCount()
    {
        return $this->sum('sell');
    }

}
