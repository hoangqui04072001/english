<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class VideoProduct extends Model
{
    use HasFactory;
    protected $table ='videos_products';

    protected $fillable =[

        'products_id',
        'name',
        'video'
    ];
    public function Product():BelongsTo
    {
        return $this->belongsTo(Product::class,'products_id','id')->withDefault(['name' => 'Chua cap nhat']);
    }
}
