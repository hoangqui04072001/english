<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $category =[
            ['name'=>'A2','name_slug'=>'a-2','status'=>'1','description'=>'Tiếng Anh Cơ Bản','created_by'=>'admin','update_by'=>'admin'],
            ['name'=>'B1','name_slug'=>'b-1','status'=>'1','description'=>'Tiếng Anh Chứng Chỉ','created_by'=>'admin','update_by'=>'admin'],
            ['name'=>'B2','name_slug'=>'b-2','status'=>'1','description'=>'Tiếng Anh Chứng Chỉ','created_by'=>'admin','update_by'=>'admin'],
            ['name'=>'C1','name_slug'=>'c-1','status'=>'1','description'=>'Tiếng Anh Chứng Chỉ','created_by'=>'admin','update_by'=>'admin'],
            ['name'=>'Ielts','name_slug'=>'ielts','status'=>'0','description'=>'Tiếng Anh Quốc Tế','created_by'=>'admin','update_by'=>'admin']
        ];
        foreach($category as $item)
        {
            Category::updateOrCreate($item);
        }
    }
}
