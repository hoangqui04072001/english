<?php

namespace Database\Seeders;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
       $roles =[
        ['name'=> 'Admin',   'display_name'=>'Quản Trị Viên','group'=>'system'],
        ['name'=> 'Teacher', 'display_name'=>'Giáo Viên','group'=>'watch'],
        ['name'=> 'Student', 'display_name'=>'Nhân Viên','group'=>'watch'],
        ['name'=> 'Customer', 'display_name'=>'Giao Vien','group'=>'watch'],
     
    ];

    foreach($roles as $role){
        Role::updateOrCreate($role);
        }
    }
}
