<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permission =[
            ['name'=>'create','display_name'=>'Tạo','group'=>'create_group','guard_name'=>'web'],
            ['name'=>'destroy','display_name'=>'Xóa','group'=>'destroy_group','guard_name'=>'web'],
            ['name'=>'edit','display_name'=>'sửa','group'=>'edit_group','guard_name'=>'web'],
            ['name'=>'sreach','display_name'=>'tìm kiếm','group'=>'sreach_group','guard_name'=>'web'],
            ['name'=>'read','display_name'=>'đọc','group'=>'read_group','guard_name'=>'web']
        ];
        foreach($permission as $item){
            Permission::updateOrCreate($item);
            
        }
        // xong rồi chạy lệnh ra
        // php artisan db:seed --class=PermissionDatabaseSeeder
    }
}
