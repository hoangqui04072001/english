<?php

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('tên loại khóa học');
            $table->string('name_slug')->comment('tên loại khóa học slug');
            $table->integer('status')->comment('trạng thái');
            $table->string('description')->comment('mô tả');
            $table->string('created_by')->comment('người tạo');
            $table->string('update_by')->nullable()->comment('người cập nhật');
            $table->timestamps();
            $table->engine = 'InnoDB';


        });

        // Schema::create('categories_products', function (Blueprint $table)
        // {
        //     $table->id();
        //     // $table->unsignedBigInteger('product_id')->comment('khóa ngoại khóa học ');
        //     // $table->unsignedBigInteger('category_id')->comment('khóa ngoại loại khóa học');

        //     $table->foreignIdFor(Product::class)->constrained('id')->cascadeOnDelete();
        //     $table->foreignIdFor(Category::class)->constrained('id')->cascadeOnDelete();
        //     $table->timestamps();
        //     $table->engine = 'InnoDB';

        // });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Schema::dropIfExists('categories_products');
        Schema::dropIfExists('categories');
    }
};
