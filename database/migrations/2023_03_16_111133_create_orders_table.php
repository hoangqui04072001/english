<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->foreignId('user_id')->constrained('users');
            $table->string('status')->default(0)->comment('Trạng thái');
            $table->string('type')->default(1)->comment('1: Khóa học có sẵn, 2: Khóa học trực tiếp');
            $table->string('order_name')->comment('Tên người đặt khóa học');
            $table->string('order_phone')->comment('Số điện thoại người đặt khóa học');
            $table->string('order_email')->comment('gmail người đặt khóa học');
            $table->string('order_gender')->nullable()->comment('Giới tính 1:Nam, 2: Nữ, 3: Khác');
            $table->string('order_address')->nullable()->comment('Địa chỉ');
            $table->integer('total');
            $table->timestamps();
            $table->engine='InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
