<?php

use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->id();
            $table->integer('sellnumber');
            $table->double('sell');
            $table->foreignIdFor(Order::class)->constrained()->cascadeOnDelete()->comment('id đơn hàng');
            $table->foreignIdFor(Product::class)->constrained()->cascadeOnDelete()->comment('id khóa học');
            $table->timestamps();
            $table->engine='InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders_producs');
    }
};
