<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id(); 
            $table->foreignId('category_id')->constrained('categories')->comment('loại khóa học');
            $table->foreignId('teacher_id')->constrained('teachers')->comment('tên giáo viên');
            $table->string('name')->comment('tên khóa học');
            $table->string('name_slug')->nullable()->comment('tên khóa học slug');
            $table->string('description')->comment(' thông tin của khóa học');
            $table->double('sell')->comment('Giá bán');
            $table->integer('number')->comment('số lượng khóa học');
            $table->string('picture')->nullable()->comment('ảnh khóa học');
            $table->integer('created_by')->nullable()->comment('Người tạo');
            $table->integer('updated_by')->nullable()->comment('Người cập nhật');
            $table->timestamps();
            $table->engine = 'InnoDB';
            
        });
        
        Schema::create('videos_products', function (Blueprint $table) {
            $table->id(); 
            $table->string('name')->comment('tên video');
            $table->string('video')->nullable()->comment('Đường dẫn liên kết video');
            
            $table->foreignId('products_id')->constrained('products')->comment('lk khóa ngoại');
            $table->timestamps();
            $table->engine = 'InnoDB';
        
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('videos_products');
        Schema::dropIfExists('products');
       
    }
};
