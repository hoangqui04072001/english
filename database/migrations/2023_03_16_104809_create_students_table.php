<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->string('name')->nullable()->comment('tên học viên');
            $table->string('code')->unique()->comment('mã học viên');
            $table->string('phone')->nullable()->comment('số điện thoại');
            $table->string('email')->nullable()->comment('email học viên');
            $table->date('birth_date')->nullable()->comment('ngày sinh');
            $table->string('gender')->default(1)->comment('giới tính 1: Nam, 2: nữ, 3: Khác');
            $table->string('created_by')->comment('Người tạo');
            $table->string('updated_by')->nullable()->comment('Người cập nhật');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('students');
    }
};
