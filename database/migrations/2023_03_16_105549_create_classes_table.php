<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Tên lớp học');
            $table->string('code')->unique()->comment('Mã lớp');
            $table->string('status')->default(1)->comment('Trạng thái');
            $table->text('description')->nullable()->comment('Mô tả');
            $table->integer('sessions_number')->comment('Số buổi học');
            $table->foreignId('calendar_id')->constrained('calendars')->comment('lk buổi học');
            $table->foreignId('teacher_id')->constrained('teachers')->comment('lk Giáo viên');
            $table->foreignId('product_id')->constrained('products')->comment('lk Khóa học');
            $table->integer('quantity')->comment('Số lượng học viên');
            $table->date('start_date')->nullable()->comment('Thời gian bắt đầu');
            $table->date('end_date')->nullable()->comment('Thời gian kết thúc');
            $table->string('created_by')->comment('Người tạo');
            $table->string('updated_by')->nullable()->comment('Người cập nhật');

            $table->timestamps();
            $table->engine ='InnoDB';
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('classes');
    }
};
