<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->string('name')->comment('tên giáo viên');
            $table->string('email')->unique()->comment('email giáo viên');
            $table->string('address')->comment('địa chỉ');
            $table->date('birth_date')->nullable()->comment('ngày sinh');
            $table->integer('status')->comment('trạng thái');
            $table->string('phone')->unique()->comment('số điện thoại');
            $table->string('level')->comment('trình độ');
            $table->string('created_by')->comment('Người tạo');
            $table->string('updated_by')->nullable()->comment('Người cập nhật');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('teachers');
    }
};
