<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\PermissionRegistrar;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('address')->nullable()->comment('Địa chỉ');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('cccd')->nullable()->comment('căn cước nhân dân');
            $table->date('birth_date')->nullable()->comment('Ngày sinh');
            $table->string('phone')->nullable()->comment('Số điện thoại');
            $table->string('gender')->nullable()->comment('Giới Tính');
            $table->string('type')->comment('1: admin, 2: member');
            $table->rememberToken();
            $table->timestamps();
            $table->engine = 'InnoDB';
            
        });
        DB::table('users')->insert([
            'name'=>'Hoàng Quý',
             'cccd'=>'123456789',
             'email'=>'hoangqui04072001@gmail.com',
             'password'=>Hash::make('12345678'),
             'type'=>'admin',
        ]);
       
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    { 
       
        Schema::dropIfExists('users');
    }
};
